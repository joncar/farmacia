<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function blog_footer(){
		$this->db->order_by('fecha','DESC');	
		$this->db->limit(2);	
		$galeria = $this->db->get('blog');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/blog/'.$g->foto);
			$galeria->row($n)->titulo = cortar_palabras($galeria->row($n)->titulo,7).'...';
			$galeria->row($n)->fecha = strftime('%B %d, %Y',strtotime($g->fecha));
			$galeria->row($n)->link = base_url('blog/'.toURL($g->id.'-'.$g->titulo));
		}
		return $galeria;
	}

	function categoria_galeria(){		
		$galeria = $this->db->get('categoria_galeria');		
		return $galeria;
	}

	function galeria(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_equipo(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria_equipo');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria_equipo/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_que_es_mif(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria_que_es_mif');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria_que_es_mif/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_que_incluye(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria_que_incluye');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria_que_incluye/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_resort(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria_resort');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria_resort/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_excursiones(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria_excursiones');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria_excursiones/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_actividades(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria_actividades');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria_actividades/'.$g->foto);
		}
		return $galeria;
	}

	function getServicios($where = array()){
		empty($where)?$where['blog_categorias_id'] = 9:$where;
		$link = $_SESSION['lang']=='es'?'servicios':'serveis';
		$where['blog.idioma'] = 'ca';
		$servicios = $this->db->get_where('blog',$where);
		foreach($servicios->result() as $n=>$v){
			$servicios->result_object[$n] = $this->traduccion->traducirObj($v);
			$servicios->row($n)->link = empty($v->url)?base_url($link.'/'.toUrl($v->id.'-'.$v->titulo)):base_url($link.'/'.$v->url);
		}
		return $servicios;
	}
}