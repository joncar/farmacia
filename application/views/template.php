<!doctype html>
<html lang="es">
	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($description) ?'': $description ?>" /> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<script>var URL = '<?= base_url() ?>';</script>

	<?php if(!empty($css_files)):?>
	      <?php foreach($css_files as $file): ?>
	      <link href="<?= $file ?>" rel="stylesheet">
	      <?php endforeach; ?>                
	<?php endif; ?>
	<!-- Stylesheet -->
	<link href="<?= base_url() ?>theme/theme/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>theme/theme/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>theme/theme/css/animate.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>theme/theme/css/css-plugin-collections.css" rel="stylesheet"/>
	<link href="<?= base_url() ?>theme/theme/css/menuzord-megamenu.css" rel="stylesheet"/>
	<link id="menuzord-menu-skins" href="<?= base_url() ?>theme/theme/css/menuzord-skins/menuzord-boxed_custom.css" rel="stylesheet"/>
	<link href="<?= base_url() ?>theme/theme/css/style-main-custom.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>theme/theme/css/preloader.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>theme/theme/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>theme/theme/css/responsive.css" rel="stylesheet" type="text/css">
	<link  href="<?= base_url() ?>theme/theme/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
	<link  href="<?= base_url() ?>theme/theme/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
	<link  href="<?= base_url() ?>theme/theme/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url() ?>theme/theme/css/colors/theme-skin-color-set3.css" rel="stylesheet" type="text/css">
	<script src="<?= base_url() ?>theme/theme/js/jquery-2.2.4.min.js"></script>
	<script src="<?= base_url() ?>theme/theme/js/jquery-ui.min.js"></script>
	<script src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>theme/theme/js/jquery-plugin-collection.js"></script>
	<script src="<?= base_url() ?>theme/theme/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
	<script src="<?= base_url() ?>theme/theme/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  	<script src="https://www.google.com/recaptcha/api.js?render=6Lc50o8UAAAAAPxrPuFAy2MlbQ-GszUJT_TpM-0Z"></script>
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">

	<div id="wrapper">
		  <!-- preloader -->
		  <div id="preloader">
		    <div id="spinner">
		      <div class="preloader-dot-loading">
		        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
		      </div>
		    </div>
		    <!--<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>-->
		  </div>

		<?php 
			if(empty($editor)){
				$this->load->view($view); 
			}else{
				echo $view;
			}
		?>	
		<?php $this->load->view('includes/template/scripts') ?>
		<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
	</div>
</body>
</html>