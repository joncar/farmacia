
<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="<?= base_url() ?>theme/theme/js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc"></script>
<script src="<?= base_url() ?>theme/theme/js/google-map-init-multilocation.js"></script>
<script src="<?= base_url() ?>js/frame.js"></script>

<?php if(!empty($js_files)):?>
  <?php foreach($js_files as $file): ?>
  	<script src="<?= $file ?>"></script>
  <?php endforeach; ?>                
<?php endif; ?>