<?php
$this->set_css($this->default_theme_path . '/bootstrap2/bootstrap/css/bootstrap.css');
$this->set_css($this->default_theme_path . '/bootstrap2/css/flexigrid.css');
$this->set_js_lib($this->default_javascript_path . '/' . grocery_CRUD::JQUERY);
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . '/bootstrap2/bootstrap/js/bootstrap.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/cookies.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/flexigrid.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/pagination.js');
/** Fancybox */
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');

/** Jquery UI */
$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">    
    <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
        <div class="row">
            
            <div class="col-xs-6">
                <div id="dynamic-table_filter" class="dynamic-table_length">
                    <div class="dt-buttons btn-overlap btn-group">
                        <?php if(!$unset_add):?>
                            <a href='<?php echo $add_url?>' title='<?php echo $this->l('list_add'); ?> <?php echo $subject?>' class="dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                                <span>
                                    <i class="fa fa-plus-circle bigger-110 green"></i> 
                                </span>
                            </a>
                        <?php endif; ?>
                        <button title='Realizar busqueda' type="button" class="searchActionClick dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                            <span>
                                <i class="fa fa-search bigger-110 blue"></i>
                            </span>
                        </button>
                        <button title='Refrescar listado'  type="button" class="ajax_refresh_and_loading dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                            <span>
                                <i class="ace-icon fa fa-refresh bigger-110 grey"></i>
                            </span>
                        </button>
                        <?php if(!$unset_export): ?>
                            <a title="exportar a excel" href="<?php echo $export_url; ?>" target="_blank" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                                <span>
                                    <i class="fa fa-file-excel-o bigger-110 orange"></i> 
                                </span>
                            </a>
                        <?php endif; ?>
                        <?php if(!$unset_print): ?>
                            <a title="imprimir listado" href="<?php echo $print_url; ?>" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                                <span>
                                    <i class="fa fa-print bigger-110 grey"></i> 
                                </span>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="dataTables_length" id="dataTables_filter" style="text-align:right">
                    <label>Mostrando 
                        <select name="per_page" id='per_page'  aria-controls="dynamic-table" class="form-control input-sm per_page">                        
                            <?php foreach ($paging_options as
                                    $option) { ?>
                                <option value="<?php echo $option; ?>" <?php if ($option == $default_per_page) { ?>selected="selected"<?php } ?>><?php echo $option; ?>&nbsp;&nbsp;</option>
<?php } ?>
                        </select> por pagina
                    </label>
                </div>
            </div>
        </div>

            <?php if (!empty($list)): ?>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer">                        
                    <thead>
                        <tr role="row">                    
                                <?php foreach ($columns as
                                        $column): ?>
                                <th class="field-sorting sorting" rel='<?php echo $column->field_name ?>'>
                                    <?php echo $column->display_as ?>                        
                                    <span id="th_<?= $column->field_name ?>"></span>
                                    <?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>
                                        <?php if ($order_by[1] == 'asc'): ?>
                                            <i class="fa fa-arrow-up"></i>                        
                                        <?php else: ?>
                                            <i class="fa fa-arrow-down"></i>
                                    <?php endif; ?>
                                <?php } ?>
                                </th>
                    <?php endforeach ?>
                        <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)): ?>
                                <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                        <div class="text-right">
                        <?php echo $this->l('list_actions'); ?>
                        </div>
                        </th>
    <?php endif ?>
                    </tr>
                    <tr class="hide searchRow">                   
                            <?php foreach($columns as $column):?>
                            <th>
                                <?php if(!in_array($column->field_name,$unset_searchs)): ?>
                                    <input type="hidden" name="search_field[]" value="<?= $column->field_name ?>">
                                    <?php
                                       $value = '';
                                       if(!empty($_POST['search_text']) && !empty($_POST['search_field'])){
                                           foreach($_POST['search_field'] as $n=>$v){
                                               if($v==$column->field_name && !empty($_POST['search_text'][$n])){
                                                   $value = $_POST['search_text'][$n];
                                               }
                                           }
                                       }
                                       echo form_input('search_text[]',$value,'style="width:100%" class="form-control" placeholder="Filtrar por '.$column->display_as.'"') 
                                    ?>
                                <?php endif ?>
                            </th>
                            <?php endforeach?>                
                    </tr>
                    </thead>
                    <tbody class="ajax_list">
                        <?php echo $list_view?>
                    </tbody>
                </table>
            </div>
            <?php else: ?>
    Sin datos para mostrar
            <?php endif; ?>



        <div class="row">
            <div class="col-xs-6">
                <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
					<?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
					<?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
					<?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
					<?php
						echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')
					);
					?>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="dataTables_paginate paging_simple_numbers pageContent" id="dynamic-table_paginate">
                    <ul class="pagination">                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>
