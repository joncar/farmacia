<?php	
	$this->set_js_lib('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
	$this->set_js_config('assets/grocery_crud/themes/bootstrap2/js/flexigrid-add.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $insert_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
	<?php get_instance()->load->view(get_instance()->theme.'servicios_pedidos',array('hidden_fields'=>$hidden_fields,'input_fields'=>$input_fields),FALSE,'paginas'); ?>
<?php echo form_close(); ?>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
        $("input[type='text'],input[type='password']").addClass('form-control');
</script>
