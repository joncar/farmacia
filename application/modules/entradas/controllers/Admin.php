<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog($x = ''){
            $crud = $this->crud_function('','');
            $crud->field_type('blog_categorias_id','hidden',10);
            $crud->where('blog.blog_categorias_id',10);
            $crud->set_relation('blog_subcategorias_id','blog_subcategorias','nombre',array('blog_categorias_id'=>10));            
            $crud->fields('blog_categorias_id','blog_subcategorias_id','foto','url','titulo','texto','fecha','user','idioma','tags','vistas','status','publicar_desde','idiomas');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'1804px','height'=>'1200px'));
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán'/*,'es'=>'Castellano','en'=>'Ingles'*/));
            $crud->field_type('idiomas','hidden');
            if($crud->getParameters()=='add'){            
                $crud->set_rules('url','URL','required|is_unique[blog.url]');                
            }
            if($crud->getParameters()!='list'){
                $crud->display_as('url','Titulo de entrada personalizada para la URL, (EJ: <b>servicio-de-materiales</b>)');
            }
            $crud->columns("foto","titulo","tags","fecha","idioma");            
            $crud->field_type('user','string',$this->user->nombre);
            $crud->add_action('<i class="fa fa-image"></i> Traducir','',base_url('entradas/admin/blog/traducir').'/');            
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function servicios(){
        	$this->as['servicios'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('blog_categorias_id','hidden',9);
            $crud->where('blog.blog_categorias_id',9);
            //$crud->where('blog.idioma','ca');
            $crud->field_type('blog_subcategorias_id','hidden',1);                        
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'1804px','height'=>'1200px'));
            $crud->field_type('foto_lateral','image',array('path'=>'img/blog','width'=>'325px','height'=>'325px'));    
            $crud->field_type('banner_main','image',array('path'=>'img/blog','width'=>'1804px','height'=>'1200px')); 
            $crud->field_type('slider_main','image',array('path'=>'img/blog','width'=>'1920px','height'=>'873px'));       
            $crud->field_type('icono','image',array('path'=>'img/blog','width'=>'30px','height'=>'30px'));              
            $crud->field_type('tags','hidden');            
			$crud->field_type('fecha','hidden');
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán'/*,'es'=>'Castellano','en'=>'Ingles'*/));
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('idiomas','hidden');
            if($crud->getParameters()=='add'){            
                $crud->set_rules('url','URL','required|is_unique[blog.url]');                
            }
            if($crud->getParameters()!='list'){
                $crud->display_as('url','Titulo de entrada personalizada para la URL, (EJ: <b>servicio-de-materiales</b>)');
            }
            $crud->set_clone();
            $crud->set_field_upload('icono_main','img/blog');
            $crud->display_as('status','Mostrar formulario de pedidos');
            $crud->columns("foto","titulo","idioma");            
            $crud->add_action('<i class="fa fa-image"></i> Marcas','',base_url('entradas/admin/marcas').'/');            
            $crud->add_action('<i class="fa fa-image"></i> Traducir','',base_url('entradas/admin/servicios/traducir').'/');            
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function equipo(){
            $this->as['equipo'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('blog_categorias_id','hidden',11);
            $crud->field_type('blog_subcategorias_id','hidden',3);                        
            $crud->where('blog.blog_categorias_id',11);
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'325px','height'=>'325px'));
            $crud->fields('blog_categorias_id','blog_subcategorias_id','titulo','descripcion_corta','texto','foto','idioma','orden');
            $crud->columns("foto","titulo","idioma");            
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));                                    
            if($crud->getParameters()=='add'){            
                $crud->set_rules('url','URL','required|is_unique[blog.url]');                
            }
            if($crud->getParameters()!='list'){
                $crud->display_as('url','Titulo de entrada personalizada para la URL, (EJ: <b>servicio-de-materiales</b>)');
            }
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function responsables(){
            $this->as['responsables'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->fields('blog_categorias_id','blog_subcategorias_id','titulo','descripcion_corta');
            $crud->columns('blog_categorias_id','blog_subcategorias_id','titulo','descripcion_corta');
            $crud->set_relation('titulo','blog','titulo',array('blog_categorias_id'=>9));
            $crud->set_relation('descripcion_corta','blog','titulo',array('blog_categorias_id'=>11));
            $crud->field_type('blog_categorias_id','hidden',12);
            $crud->field_type('blog_subcategorias_id','hidden',4); 
            $crud->where('blog.blog_categorias_id',12);   
            $crud->display_as('descripcion_corta','Persona');                    
            $crud->display_as('titulo','Servicio');    
            if($crud->getParameters()=='add'){
                $crud->set_rules('url','URL','required|is_unique[blog.url]');                
            }
            if($crud->getParameters()!='list'){
                $crud->display_as('url','Titulo de entrada personalizada para la URL, (EJ: <b>servicio-de-materiales</b>)');
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function pedidos_recetas(){            
            $crud = $this->crud_function('','');
            $crud->set_field_upload('receta1','files')
                 ->set_field_upload('receta2','files')
                 ->set_field_upload('receta3','files');       
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function marcas(){
            $this->load->library('image_crud');
            $crud = new image_crud();
            $crud->set_table('marcas')
                 ->set_image_path('img/blog')
                 ->set_url_field('foto')
                 ->set_relation_field('blog_id');
            $crud->module = 'entradas';
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>