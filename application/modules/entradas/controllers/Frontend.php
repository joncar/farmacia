<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->lang->load('contacto', $this->fullLangs[$_SESSION['lang']]);        
        }
        
        public function servicios($id = ''){
            $link = $_SESSION['lang']=='es'?'servicios':'serveis';                        
            if(empty($id)){ //List
                if($_SERVER['REDIRECT_QUERY_STRING']!=$link){
                    redirect(base_url($link));
                }
                $this->db->order_by('orden','ASC');
                $servicios = $this->db->get_where('blog',array('blog_categorias_id'=>9,'idioma'=>'ca'));

            	//$servicios = $this->db->get_where('blog',array('blog_categorias_id'=>9,'idioma'=>$_SESSION['lang']));
            	foreach($servicios->result() as $n=>$s){
                    $servicios->result_object[$n] = $this->traduccion->traducirObj($s);
            		$servicios->row($n)->link = empty($s->url)?base_url($link.'/'.toUrl($s->id.'-'.$s->titulo)):base_url($link.'/'.$s->url);
            		$servicios->row($n)->foto = base_url('img/blog/'.$s->foto);
            		$servicios->row($n)->foto_lateral = base_url('img/blog/'.$s->foto_lateral);            		
                    $servicios->row($n)->texto = cortar_palabras(strip_tags($s->texto),10);
            	}
                
                $this->loadView(array('view'=>'servicios','servicios'=>$servicios,'title'=>ucfirst($link)));
            }else{ // View
                $url = $id;
                $id = explode('-',$id);
                $id = $id[0];

                if(is_string($id)){
                    $this->db->where('url',$url);
                    $this->db->or_where('getUrlEs(blog.id)',$url);
                    $blog = $this->db->get_where('blog');
                    if($blog->num_rows()>0){
                        $id = $blog->row()->id;
                    }
                }                

                if(is_numeric($id)){
                    $servicio = $this->db->get_where('blog',array('id'=>$id));
                    if($servicio->num_rows()>0){
                        $servicio = $servicio->row();
                        $entrada = $this->traduccion->traducirObj($servicio);
                        if(explode('/',$_SERVER['REDIRECT_QUERY_STRING'])[0]!=$link){
                            $enlace = empty($entrada->url)?base_url($link.'/'.toUrl($entrada->id.'-'.$entrada->titulo)):base_url($link.'/'.$entrada->url);
                            redirect($enlace);
                        }
                        $servicio->link = empty($servicio->url)?base_url($link.'/'.toUrl($servicio->id.'-'.$servicio->titulo)):base_url($link.'/'.$servicio->url);
                        $servicio->foto = base_url('img/blog/'.$servicio->foto);
                        $servicio->foto_lateral = base_url('img/blog/'.$servicio->foto_lateral);                        
                        $servicio->marcas = $this->db->get_where('marcas',array('blog_id'=>$id));
                        foreach($servicio->marcas->result() as $n=>$s){
                            $servicio->marcas->row($n)->foto = base_url('img/blog/'.$s->foto);
                        }
                        $this->db->order_by('orden','ASC');
                        $servicios = $this->db->get_where('blog',array('blog_categorias_id'=>9,'idioma'=>'ca','id!='=>$id));
                        foreach($servicios->result() as $n=>$s){
                            $servicios->row($n)->link = base_url($link.'/'.toUrl($s->id.'-'.$s->titulo));
                            $servicios->row($n)->foto = base_url('img/blog/'.$s->foto);
                            $servicios->row($n)->foto_lateral = base_url('img/blog/'.$s->foto_lateral);                            
                            $servicios->row($n)->texto = cortar_palabras(strip_tags($s->texto),10);
                        }
                        $output = $this->pedidos_recetas(1,$id);
                        $servicio = $this->traduccion->traducirObj($servicio);
                        $servicios = $this->traduccion->transform($servicios);                        
                        $this->loadView(array('view'=>'servicios-detalles','detail'=>$servicio,'categorias'=>$servicios,'title'=>strip_tags($servicio->titulo),'output'=>$output->output,'css_files'=>$output->css_files,'js_files'=>$output->js_files,'description'=>cortar_palabras(strip_tags($servicio->texto),10)));
                    }else{
                        throw new Exception('No se encuentra la entrada solicitada',404);    
                    }
                }else{
                    throw new Exception('No se encuentra la entrada solicitada',404);
                }
            } //End view
        }

        public function equipo($id = ''){            
            $link = $_SESSION['lang']=='es'?'equipo':'equip';           
            if(empty($id)){ //List
                if($_SERVER['REDIRECT_QUERY_STRING']!=$link){
                    redirect(base_url($link));
                }
                $this->db->order_by('orden','ASC');
                $equipo = $this->db->get_where('blog',array('blog_categorias_id'=>11,'idioma'=>$_SESSION['lang']));
                foreach($equipo->result() as $n=>$s){
                    $equipo->row($n)->link = base_url($link.'/'.toUrl($s->id.'-'.$s->titulo));
                    $equipo->row($n)->foto = base_url('img/blog/'.$s->foto);
                    $equipo->row($n)->foto_lateral = base_url('img/blog/'.$s->foto_lateral);                 
                    $equipo->row($n)->texto = cortar_palabras(strip_tags($s->texto),10);
                }
                $this->loadView(array('view'=>'equipo','equipo'=>$equipo,'title'=>ucfirst($link)));
            }else{ // View
                $url = $id;
                $id = explode('-',$id);
                $id = $id[0];
                if(is_string($id)){
                    $blog = $this->db->get_where('blog',array('url'=>$url));
                    if($blog->num_rows()>0){
                        $id = $blog->row()->id;
                    }
                }
                if(is_numeric($id)){
                    
                    $servicio = $this->db->get_where('blog',array('id'=>$id));
                    if($servicio->num_rows()>0){
                        $servicio = $servicio->row();
                        $entrada = $this->traduccion->traducirObj($servicio);
                        if(explode('/',$_SERVER['REDIRECT_QUERY_STRING'])[0]!=$link){
                            $enlace = empty($entrada->url)?base_url($link.'/'.toUrl($entrada->id.'-'.$entrada->titulo)):base_url($link.'/'.$entrada->url);
                            redirect($enlace);
                        }
                        $servicio->link = base_url($link.'/'.toUrl($servicio->id.'-'.$servicio->titulo));
                        $servicio->foto = base_url('img/blog/'.$servicio->foto);
                        $servicio->foto_lateral = base_url('img/blog/'.$servicio->foto_lateral);                        
                        $servicio->marcas = $this->db->get_where('marcas',array('blog_id'=>$id));
                        foreach($servicio->marcas->result() as $n=>$s){
                            $servicio->marcas->row($n)->foto = base_url('img/blog/'.$s->foto);
                        }
                        $this->db->order_by('orden','ASC');
                        $equipo = $this->db->get_where('blog',array('blog_categorias_id'=>9,'idioma'=>$_SESSION['lang'],'id!='=>$id));
                        foreach($equipo->result() as $n=>$s){
                            
                            $equipo->row($n)->link = base_url($link.'/'.toUrl($s->id.'-'.$s->titulo));
                            $equipo->row($n)->foto = base_url('img/blog/'.$s->foto);
                            $equipo->row($n)->foto_lateral = base_url('img/blog/'.$s->foto_lateral);                            
                            $equipo->row($n)->texto = cortar_palabras(strip_tags($s->texto),10);
                        }
                        $this->loadView(array('view'=>'equipo-detalles','detail'=>$servicio,'categorias'=>$equipo,'title'=>$servicio->titulo,'description'=>cortar_palabras(strip_tags($servicio->texto),10)));
                    }else{
                        throw new Exception('No se encuentra la entrada solicitada',404);    
                    }
                }else{
                    throw new Exception('No se encuentra la entrada solicitada',404);
                }
            } //End view
        }

        function pedidos_recetas($return = '',$x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();            
            $crud->set_table('pedidos_recetas')
                 ->set_theme('pedidos')
                 ->set_subject('Pedidos')
                 ->set_field_upload('receta1','files')
                 ->set_field_upload('receta2','files')
                 ->set_field_upload('receta3','files')
                 ->field_type('blog_id','hidden',$x)
                 ->field_type('fecha','string')
                 ->unset_back_to_list();
            $crud->set_rules('nombre','Nombre','required|callback_validar_politicas');
            $crud->set_lang_string('insert_success_message',$this->lang->line('medicacio_success'));
            $crud->set_url('entradas/frontend/pedidos_recetas/');
            $crud->required_fields_array();
            $crud->callback_before_insert(function($post){
                $post['fecha_solicitud'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->callback_after_insert(function($post){                
                $post['extras'] = '<p class="MsoNormal"><b><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">Solicitud de formulas</b></p>';
                foreach($post as $n=>$p){
                    if($n!='blog_id' && $n!='aviso' && $n!='politica' && $n!='asunto'){
                        $n = $this->lang->line('solicitud_formula_'.$n);
                        if(!empty($n)){
                            $post['extras'].= '<p class="MsoNormal"><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">'.$n.':</span></strong> '.$p.'</p>';
                        }
                    }
                }
                get_instance()->load->library('mailer');
                if(!empty($post['receta1'])){
                    get_instance()->mailer->mail->addAttachment('files/'.$post['receta1']);
                }
                if(!empty($post['receta2'])){
                    get_instance()->mailer->mail->addAttachment('files/'.$post['receta2']);
                }
                if(!empty($post['receta3'])){
                    get_instance()->mailer->mail->addAttachment('files/'.$post['receta3']);
                }
                $post['titulo'] = $this->lang->line('solicitud_medicacio');
                $post['asunto2'] = $this->lang->line('solicitud_asunto2');
                $post['to'] = get_instance()->db->get('ajustes')->row()->email_formulas;
                $this->enviarcorreo((object)$post,1,$post['to']);   
                $post['asunto2'] = $this->lang->line('solicitud_gracias');
                $this->enviarcorreo((object)$post,1,$post['email']);                
            });
            if($return == 1){
                $crud = $crud->render(2,'application/modules/entradas/views/');
                return $crud;
            }
            $crud = $crud->render();
        }

        function validar_politicas(){
            if(empty($_POST['politica'])){
                $this->lang->load('contacto', $this->fullLangs[$_SESSION['lang']]);                
                $this->form_validation->set_message('validar_politicas',$this->lang->line('politicas'));
                return false;
            }   
        }
    }
?>