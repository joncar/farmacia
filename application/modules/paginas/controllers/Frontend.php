<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){            
            $theme = $this->theme;
            $params = $this->uri->segments;
            //Existe?            
            if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                $page = $this->load->view($theme.$url,array(),TRUE);
            }else{                                
                //Verificar si existe en otros idiomas
                $idiomas = $this->db->get('ajustes')->row()->idiomas;
                $idiomas = explode(',',$idiomas);
                foreach($idiomas as $i){
                    $i = trim($i);
                    if(file_exists(APPPATH.'modules/paginas/views/'.$i.'/'.$url.'.php')){                    
                        if($i!=$this->theme){//Verificar si hay que traducir
                            $traducciones = $this->db->get_where('traducciones',array('original'=>$url,'idioma'=>$_SESSION['lang']));
                            if($traducciones->num_rows()>0){
                                $url = $traducciones->row()->traduccion;
                                if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                                    header("Location:".base_url().$url.'.html');
                                    die();
                                }    
                            }
                        }
                        $page = $this->load->view($i.'/'.$url,array(),TRUE);
                        $_SESSION['lang'] = $i;
                        $this->theme = $i.'/';
                    }
                }
                
            }
            if(empty($page)){
                throw new exception('Pagina web no encontrada',404);
            }
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$page,
                    'link'=>$url,
                    'url'=>$url,
                    'description'=>cortar_palabras(strip_tags($page),10),
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){       
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){     
            $this->lang->load('contacto', $this->fullLangs[$_SESSION['lang']]);        
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');            
            $this->form_validation->set_rules('politicas','Politicas','required');            
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                    $_SESSION['msj'] = $this->error($this->lang->line('error_response_captcha_form'));
                }else{                    
                    $datos = $_POST;                                    
                    $datos['titulo'] = $this->lang->line('titulo');                    
                    $datos['asunto2'] = $this->lang->line('asunto2');
                    $datos['asunto3'] = $this->lang->line('asunto3');
                    if(!empty($_POST['extras'])){
                        unset($datos['extras']);
                        $datos['extras'] = '';
                        foreach($_POST['extras'] as $n=>$v){
                            $n = $this->lang->line($n);
                            $datos['extras'].= '<p class="MsoNormal"><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">'.$n.':</span></strong> '.$v.'</span></p>';                            
                        }
                    }else{
                        $datos['extras'] = '';
                    }

                    if(empty($_POST['asunto'])){
                        $datos['asunto'] = '';
                    }
                    $this->db->insert('contacto',array(
                        'nombre'=>$_POST['nombre'],
                        'email'=>$_POST['email'],
                        'telefono'=>$_POST['extras']['telefono'],
                        'asunto'=>$_POST['extras']['asunto'],
                        'mensaje'=>$_POST['extras']['mensaje'],
                        'fecha_solicitud'=>date("Y-m-d H:i:s")
                    ));
                    $email = $this->db->get('ajustes')->row()->email_contactenos;
                    if(!empty($_POST['to'])){
                        unset($datos['to']);
                        $email = $_POST['to'];
                    }else{
                        $datos['to'] = $email;
                    }
                    


                    $this->enviarcorreo((object)$datos,1,$email);
                     
                    $this->enviarcorreo((object)$datos,12,$datos['email']);
                    $_SESSION['msj'] = $this->success($this->lang->line('success_response_contact_form'));
                }
            }else{                
               $_SESSION['msj'] = $this->error($this->lang->line('error_response_contact_form'));
            }

            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        function subscribir(){
            $err = 'error';
            $this->lang->load('contacto', $this->fullLangs[$_SESSION['lang']]);        
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('token','Captcha','required');
            //$this->form_validation->set_rules('politicas','Políticas','required');
            if($this->form_validation->run()){                
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                    $_SESSION['msj2'] = $this->error($this->lang->line('error_response_captcha_form'));
                }else{  
                    $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                    $success = $emails->num_rows()==0?TRUE:FALSE;
                    if($success){
                        $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                        $_SESSION['msj2'] = $this->success($this->lang->line('contactO_email_subs_success'));
                        $err = 'success';
                        $_POST['titulo'] = str_replace('{email}',$_POST['email'],$this->lang->line('subscribe'));
                        $_POST['subscribe'] = str_replace('{email}',$_POST['email'],$this->lang->line('subscribeText'));
                        $_POST['subscribeBaja'] = str_replace('{email}',base64_encode($_POST['email']),$this->lang->line('subscribeBaja'));
                        $_POST['to'] = $this->db->get('ajustes')->row()->email_contactenos;
                        $this->enviarcorreo((object)$_POST,11);
                        $this->enviarcorreo((object)$_POST,11,$this->db->get('ajustes')->row()->email_contactenos);
                    }else{
                        $_SESSION['msj2'] = $this->error($this->lang->line('contacto_email_subs_error'));
                    }
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo $_SESSION['msj2'];
            unset($_SESSION['msj2']);
            //redirect(base_url().'p/contacte#subscribir');
        }
        
        function unsubscribe($email){
            $this->lang->load('contacto', $this->fullLangs[$_SESSION['lang']]);
            $email = base64_decode($email);
            $this->db->delete('subscritos',array('email'=>$email));
            correo($this->db->get('ajustes')->row()->email_contactenos,$this->lang->line('subscribeBajaTitulo'),str_replace('{email}',$email,$this->lang->line('subscribeBajaText')));
            echo $this->lang->line('subscribeBajaSuccess');
        }

        function galeria(){
            $this->loadView(array('view'=>'galeria','title'=>'Galeria','page'=>''));
        }

        function readAjax($page){
            $this->load->view($this->theme.$page);
        }

        function search(){
            $domain = base_url();
            $domain = explode('/',$domain);
            $domain = str_replace('www.','',$domain[2]);
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3A'.$domain.'+'.urlencode($_GET['q']).'&oq=site%3A'.$domain.'+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }

                $result = $_SESSION[$_GET['q']];                
                preg_match_all('@<div class=\"ZINbbc xpd O9g5cc uUPGi\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = explode('<footer>',$resultado);
                $resultado = $resultado[0];
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="ZINbbc xpd O9g5cc uUPGi">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = $r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="ZINbbc xpd O9g5cc uUPGi">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                $resultado = implode('',$resultado);
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }

        function sitemap(){
            $pages = array(
                base_url(),
                base_url().'contacto.html',
                base_url().'contacte.html',
                base_url().'historia.html',
                base_url().'equip',
                base_url().'equipo',
                base_url().'serveis',
                base_url().'servicios',
                base_url().'faq.html',
                base_url().'avis-legal.html',
                base_url().'aviso-legal.html',
                base_url().'galeria.html',
                base_url().'blog',
            );

            //Blogs
            foreach($this->db->get_where('blog',array('blog.blog_categorias_id'=>10))->result() as $b){
                $pages[] = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            }
            //Serveis
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>9))->result() as $b){
                $link = $b->idioma=='es'?'servicios':'serveis';
                $pages[] = base_url($link.'/'.toUrl($b->id.'-'.$b->titulo));
            }
            //equip
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>11))->result() as $b){
                $link = $b->idioma=='es'?'equipo':'equip';
                $pages[] = base_url($link.'/'.toUrl($b->id.'-'.$b->titulo));
            }
            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }
    }
