[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/aviso-legal.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/aviso-legal.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Aviso Legal
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	
	<!-- Section: Experts Details -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">            
            <div class="col-md-12">
              <h3 class="name font-24 mt-0 mb-0"></h3>
              <h4 class="mt-5 text-theme-colored">¿Quién es el responsable del tratamiento de sus datos personales?</h4>
              <p>ROSER MASANA CLOSA (de ahora en adelante, "FARMÀCIA MASSANA"), con domicilio en la calle Major, 95, 08788 Vilanova del Camí, (Barcelona), con N.I.F. nº 46657606J, es la responsable del tratamiento de los datos personales que usted nos proporciona y se responsabiliza de los datoss personales de acuerdo con la normativa aplicable en materia de protección de datos.</p>
              <h4 class="mt-5 text-theme-colored">¿Dónde se almacenan sus datos?</h4>
              <p>Los datos que recopilamos sobre usted se almacenan dentro del Espacio Económico Europeo («EEE»). Cualquier transferencia de sus datos personales será realizada de conformidad con las leyes aplicables.</p>
              <h4 class="mt-5 text-theme-colored">¿A quién comunicamos sus datos?</h4>
              <p>Sus datos pueden ser compartidos por FARMÀCIA MASSANA. Nunca pasamos, vendemos ni intercambiamos sus datos personales con terceros. Los datos que se mandan a terceros, se utilizarán únicamente para ofrecerle a usted nuestros servicios. Le detallaremos las categorías de terceros que acceden a sus datos para cada actividad de tratamiento específica.</p>
              <h4 class="mt-5 text-theme-colored">¿Cual es la base legal para el tratamiento de sus datos personales?</h4>
              <p>En cada tratamiento específico de datos personales recopilados sobre usted, le informaremos si la comunicación de datos personales es un requisito legal o contractual, o un requisito necesario para subscribir un contrato, y si está obligado a facilitar los datos personales, así como de las posibles consecuencias de no facilitar tales datos.</p>
              <h4 class="mt-5 text-theme-colored">¿Cuales son sus derechos?</h4>
              <p>Derecho de acceso: Cualquier persona tiene derecho a obtener confirmación sobre si en FARMÀCIA MASSANA estamos tratando datos personales que los conciernan, o no. Puede contactar a FARMÀCIA MASSANA que le remitirá los datos personales que tratamos sobre usted por correo electrónico.</p>
              <p>Derecho de portabilidad: Siempre que FARMÀCIA MASSANA procese sus datos personales a través de medios automatizados en base a su consentimiento o a un acuerdo, usted tiene derecho a obtener una copia de sus datos en un formato estructurado, de uso común y lectura mecánica transferida a su nombre o a un tercero. En ella se incluirán únicamente los datos personales que usted nos haya facilitado.</p>
<p>Derecho de rectificación: Usted tiene derecho a solicitar la rectificación de sus datos personales si son inexactos, incluyendo el derecho a completar datos que figuren incompletos.</p>
<p>Derecho de supresión: Usted tiene derecho a obtener sin más dilación indebida la supresión de cualquier dato personal suyo tratada por FARMÀCIA MASSANA a cualquier momento, excepto en las siguientes situaciones: 

<br>* tiene una deuda pendiente con FARMÀCIA MASSANA, independientemente del método de pago
<br>* se sospecha o está confirmado que ha utilizado incorrectamente nuestros servicios en los últimos cuatro años 
<br>* ha contratado algún servicio por el que conservaremos sus datos personales en relación con la transacción por normativa contable
</p>
<p>Derecho de oposición al tratamiento de datos en base al interés legítimo: Usted tiene derecho a oponerse al tratamiento de sus datos personales en base al interés legítimo de FARMÀCIA MASSANA. En este caso, FARMÀCIA MASSANA no seguirá tratando los datos personales salvo que podamos acreditar motivos legítimos imperiosos para el tratamiento que prevalezcan sobre sus intereses, derechos y libertades, o bien para la formulación, el ejercicio o la defensa de reclamaciones.</p>
<p>Derecho de oposición al marketing directo: Usted tiene derecho a oponerse al marketing directo, incluyendo la elaboración de perfiles realizada para este marketing directo. Puede desvincularse del marketing directo en cualquier momento de las siguientes maneras: siguiendo las indicaciones facilitadas a cada correo de marketing</p>
<p>Derecho a presentar una reclamación ante una autoridad de control: Si usted considera que FARMÀCIA MASSANA trata sus datos de una manera incorrecta, puede contactar con nosotros. También tiene derecho a presentar una queja ante la autoridad de protección de datos competente.</p>
<p>Derecho a limitación en el tratamiento: Usted tiene derecho a solicitar que FARMÀCIA MASSANA limite el tratamiento de sus datos personales en las siguientes circunstancias: 
<br>* si usted se opone al tratamiento de sus datos en base al interés legítimo de FARMÀCIA MASSANA. En este caso FARMÀCIA MASSANA tendrá que limitar cualquier tratamiento de estos datos a la espera de la verificación del interés legítimo. 
<br>* si usted reclama que sus datos personales son incorrectos, FARMÀCIA MASSANA tiene que limitar cualquier tratamiento de estos datos hasta que se verifique la precisión de los mismos. 
<br>* si el tratamiento es ilegal, usted podrá oponerse al hecho que se eliminen los datos personales y, en su lugar, solicitar la limitación de su uso. 
<br>* si FARMÀCIA MASSANA ya no necesita los datos personales, pero usted los necesita para la formulación, el ejercicio o la defensa de reclamaciones.</p>
<p>Ejercicio de derechos: Nos tomamos mucho debò la protección de datos y, por lo tanto, contamos con personal de servicio al cliente dedicado que se ocupa de sus solicitudes en relación con los derechos antes mencionados. Siempre puede ponerse en contacto con ellos en farmacia@farmaciamassana.cat.

</p>
              <ul class="styled-icons icon-dark icon-theme-colored2 icon-sm mt-15 mb-0">
               <!-- 
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
               <li><a href="#"><i class="fa fa-skype"></i></a></li>
               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
 -->
             </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
	

</div>
<!-- End main-content -->
[footer]