
<!-- Header -->
  <header id="header" class="header">
    
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
      
      <div class="header-top bg-theme-colored2 sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-8 hidden-xs">
            <div class="widget text-white">
              <i class="fa fa-clock-o text-white"></i> Horario:  Lun - Vie : 8.30h - 14h / 16h - 20:30h, Sábado 9h - 14h / 17h - 20:30h, Domingo 10h - 12h
            </div>
          </div>
          <div class="col-md-4">
            <div class="widget">
              <ul class="list-inline text-right flip sm-text-center">                
                <li>
                  <a class="text-white" href="[base_url]faq.html">FAQ</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="[base_url]contacto.html">Contacto</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="https://www.facebook.com/farmaciamassana/" target="_new">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="https://www.instagram.com/farmaciamassana/" target="_new">
                    <i class="fa fa-instagram"></i>
                  </a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="<?= base_url('main/traduccion/es') ?>">
                    <u>ESP</u>
                  </a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="<?= base_url('main/traduccion/ca') ?>">
                    CAT
                  </a>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </div>
    </div>
      
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue no-bg">
            <a class="menuzord-brand pull-left flip mb-15" href="<?= base_url() ?>">
              <img src="[base_url]theme/theme/images/logo-widecast.png" alt="">
            </a>
            <ul class="list-inline nav-side-icon-list pull-right">
              <li>
                <a href="#" id="inline-fullwidth-search-btn"><i class="search-icon fa fa-search"></i></a>
                <div id="inline-fullwidth-search-form" class="clearfix">
                  <form action="<?= base_url('paginas/frontend/search') ?>" method="GET">
                    <input type="text" name="q" value="" placeholder="Escriu i prem enter" autocomplete="off">
                  </form>
                  <a href="#" id="close-search-btn"><i class="icon_close"></i></a>
                </div>
              </li>
              
              <li>
                <div id="side-panel-trigger" class="side-panel-trigger">
                  <a href="#"><i class="fa fa-bars font-24"></i></a>
                </div>
              </li>
            </ul>
            <ul class="menuzord-menu">
              <li >
                <a href="<?= base_url() ?>">Inicio</a>                
              </li>
              <li>
                <a href="#">Nosotros</a>
                <ul class="dropdown">
                  <li><a href="[base_url]equipo">Equipo</a></li>
                  <li><a href="[base_url]historia.html">Historia y Valores</a></li>
                  <li><a href="[base_url]contacto.html">¿Dónde estamos?</a></li>                                    
                                                      
                </ul>
              </li>
              <li>
                <a href="[base_url]servicios">Qué ofrecemos</a>
                <ul class="dropdown">
                  <?php 
                    $this->db->order_by('orden','ASC');
                    foreach($this->elements->getServicios()->result() as $s): 
                      $s->titulo = strip_tags($s->titulo); 
                      $s->link = empty($s->url)?base_url('servicios/'.toUrl($s->id.'-'.$s->titulo)):base_url('servicios/'.$s->url);
                  ?>
                    <li><a href="<?= $s->link ?>"><?= $s->titulo ?></a></li>
                  <?php endforeach ?>
                </ul>
              </li>              
              <!--<li><a href="[base_url]botiga">Tienda</a></li>-->

              <li><a href="[base_url]galeria.html">Galería</a></li>
              <li class="activado"><a href="[base_url]blog">Noticias</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>