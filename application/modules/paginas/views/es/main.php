[menu]
<!-- Start main-content -->
  <div class="main-content"> 
   
    <!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/F9.jpg" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/F9.jpg" alt="" data-bgpositio="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Profesionales<br> 
                  <span class="text-theme-colored2">a tu servicio</span>
                  <p style="transition: none 0s ease 0s; text-align: inherit; line-height: 24px; border-width: 0px; margin: 0px 0px 10px; padding: 0px; letter-spacing: 0px; font-weight: 300; font-size: 18px;">Somos un equipo diplomado que trabaja <br>para darte el mejor servicio.</p>
                  <p>
                     <a href="[base_url]historia.html" class="btn btn-dark btn-theme-colored btn-xl">Nosotros</a> 
                     <a href="[base_url]contacto.html" class="btn btn-dark btn-theme-colored2 btn-xl">Contactar</a>
                  </p>
                </div>
              </li>


              <!-- SLIDE 2 -->
              <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/F63.jpg" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/F63.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-2-layer-3" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-textalign="['center','center','center','center']"
                  data-width="['700','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="padding-top:30px; background:rgba(255,255,255,.8); z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">Fórmulas <br> <span class="text-theme-colored2">magistrales</span>
                </div>
                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-2-layer-4" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['top','top','top','top']" data-voffset="['335','290','235','249']" 
                  data-fontsize="['18','18','16','13']"
                  data-lineheight="['30','30','28','25']"
                  data-fontweight="['600','600','600','600']"
                  data-textalign="['center','center','center','center']"
                  data-width="['700','650','600','420']"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="padding-bottom:30px; background:rgba(255,255,255,.8); z-index: 7; white-space: nowrap;">Laboratorio especializado en elaboración<br> y control de calidad de fórmulas magistrales 
                </div>
                <!-- LAYER NR. 5 -->
                <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                  id="slide-2-layer-5" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['top','top','top','top']" data-voffset="['430','370','340','340']" 
                  data-fontsize="['18','18','16','16']"
                  data-lineheight="['30','30','30','30']"
                  data-fontweight="['600','600','600','600']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 7; white-space: nowrap;"><a href="[base_url]servicios/75-formules-magistrals" class="btn btn-dark btn-theme-colored btn-xl">Saber más</a>
                </div>
              </li>
              


              <!-- SLIDE 3 -->
              <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/F62.jpg" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/F62.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-3-layer-2" 
                  data-x="['center','center','center','center']" data-hoffset="['310','200','100','0']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-width="['700','650','500','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="background: rgba(255,255,255,.8); padding: 30px 30px 0 30px; z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">Farmacia  <br><span class="text-theme-colored2">Homeopática</span>
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-3-layer-3" 
                  data-x="['center','center','center','center']" data-hoffset="['310','200','100','0']" 
                  data-y="['top','top','top','top']" data-voffset="['290','250','199','209']" 
                  data-fontsize="['18','18','16','13']"
                  data-lineheight="['30','30','28','25']"
                  data-fontweight="['600','600','600','600']"
                  data-width="['700','650','500','420']"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="background: rgba(255,255,255,.8); padding: 0 30px 30px 30px; z-index: 7; white-space: nowrap;">La homeopatía individualiza el tratamiento<br> y valora el paciente de forma global
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                  id="slide-3-layer-4" 
                  data-x="['center','center','center','center']" data-hoffset="['310','200','100','0']" 
                  data-y="['top','top','top','top']" data-voffset="['430','320','300','300']" 
                  data-fontsize="['18','18','16','16']"
                  data-lineheight="['30','30','30','30']"
                  data-fontweight="['600','600','600','600']"
                  data-width="['700','650','500','420']"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 7; white-space: nowrap;"><a href="[base_url]servicios/76-homeopatia" class="btn btn-dark btn-theme-colored btn-xl">Saber más</a>
                </div>
              </li>
              <!-- SLIDE 4 -->
              <li data-index="rs-4" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/F64.jpg" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/F64.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-2-layer-3" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-textalign="['center','center','center','center']"
                  data-width="['700','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="padding-top:30px; background:rgba(255,255,255,.8); z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">Farmacia <br> <span class="text-theme-colored2">natural</span>
                </div>
                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-2-layer-4" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['top','top','top','top']" data-voffset="['335','290','235','249']" 
                  data-fontsize="['18','18','16','13']"
                  data-lineheight="['30','30','28','25']"
                  data-fontweight="['600','600','600','600']"
                  data-textalign="['center','center','center','center']"
                  data-width="['700','650','600','420']"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="padding-bottom:30px; background:rgba(255,255,255,.8); z-index: 7; white-space: nowrap;">Productos de origen natural, ecológicos, biológicos y orgánicos <br>con la confianza y calidad farmacéutica 
                </div>
                <!-- LAYER NR. 5 -->
                <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" 
                  id="slide-2-layer-5" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['top','top','top','top']" data-voffset="['430','370','340','340']" 
                  data-fontsize="['18','18','16','16']"
                  data-lineheight="['30','30','30','30']"
                  data-fontweight="['600','600','600','600']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 7; white-space: nowrap;"><a href="[base_url]servicios/77-productes-naturals" class="btn btn-dark btn-theme-colored btn-xl">Saber más</a>
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                  keyboardNavigation:"on",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation:"off",
                  onHoverStop:"on",
                  touch:{
                    touchenabled:"on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                  }
                  ,
                  arrows: {
                    style:"zeus",
                    enable:true,
                    hide_onmobile:true,
                    hide_under:600,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    tmp:'<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                    left: {
                      h_align:"left",
                      v_align:"center",
                      h_offset:30,
                      v_offset:0
                    },
                    right: {
                      h_align:"right",
                      v_align:"center",
                      h_offset:30,
                      v_offset:0
                    }
                  },
                  bullets: {
                    enable:true,
                    hide_onmobile:true,
                    hide_under:600,
                    style:"metis",
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    direction:"horizontal",
                    h_align:"center",
                    v_align:"bottom",
                    h_offset:0,
                    v_offset:30,
                    space:5,
                    tmp:'<span class="tp-bullet-img-wrap"><span class="tp-bullet-image"></span></span>'
                  }
                },
                viewPort: {
                  enable:true,
                  outof:"pause",
                  visible_area:"80%"
                },
                responsiveLevels:[1240,1024,778,480],
                gridwidth:[1240,1024,778,480],
                gridheight:[600,550,500,450],
                lazyType:"none",
                parallax: {
                  type:"scroll",
                  origo:"enterpoint",
                  speed:400,
                  levels:[5,10,15,20,25,30,35,40,45,50],
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                  simplifyAll:"off",
                  nextSlideOnWindowFocus:"off",
                  disableFocusListener:false,
                }
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

    <!-- Section: home-boxes -->
    <section>
      <div class="container pb-0">
        <div class="section-content">
          <div class="row equal-height-inner">
            <div class="mainBan3Colores col-sm-12 col-md-4 pr-0 pr-sm-15 sm-height-auto mt-sm-0" data-margin-top="-150px">
              <div class="sm-height-auto bg-theme-colored-darker2">
                <div class="p-30">
                  <a href="[base_url]fer-comanda.html">
                      <h3 class="text-uppercase text-white mt-0">Stop&Go</h3>
                      <p class="text-white">Te lo ponemos muy fácil!<br>
                        • Nos llamas o mandas un mensaje de <b>whatsapp</b> al <b>teléfono</b> 696841650 donde nos dices lo que te interesa y a qué hora pasarás a recogerlo. 
                        <br>• Nosotros te prepararemos tu <b>pedido</b>.
                        <br>• Cuando llegues a la farmacia te <b>priorizaremos</b> el turno y así sólo tendrás que pagar.
                      </p>
                      <span class="btn btn-dark btn-theme-colored2 btn-theme-colored3 btn-xs">Más información</span>
                  </a>   
                  <div class="miniatura" style="background-image:url([base_url]theme/theme/images/3cuadros/1.png)"></div>
                </div>                
              </div>
            </div>
            <div class="mainBan3Colores col-sm-12 col-md-4 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0" data-margin-top="-150px">
              <div class="sm-height-auto bg-theme-colored2">
                <div class="p-30">
                  <h3 class="text-uppercase text-white mt-0">Servicio Whatsapp</h3>
                  <div class="opening-hours">
                    <p class="text-white">
                          • Una buena herramiento para hacer <b>consultas y pedidos</b>.
                      <br>• Para dudas sobre el medicamento (dosificaciones, formas farmacéuticas, interacciones, disponibilidad...).  
                      <br>• Para que te preparemos tu medicación, te realizamos una búsqueda de productos farmacéuticos concretos.
                      <br>• Para realizar un encargo.
                    </p>
                    <h3 class="text-white">696 84 16 50</h3>                                          
                  </div>
                  <div class="miniatura" style="background-image:url([base_url]theme/theme/images/3cuadros/2.png)"></div>
                </div>
              </div>
            </div>
            <div class="mainBan3Colores col-sm-12 col-md-4 pl-0 pl-sm-15 sm-height-auto mt-sm-0" data-margin-top="-150px">
              <div class="sm-height-auto bg-theme-colored-darker2">
                <div class="p-30">
                  <h3 class="text-uppercase text-white mt-0">Tarjeta Cliente XarxaFarma</h3>
                  <p class="text-white"> Con nuestra tarjeta cliente.<br> 
                      • Podrás utilizarla en cualquiera de las 647 farmacias del grupo Xarxafarma. <br>
                      • Obtendrás muchas <b>ventajas</b>, descuentos, promociones, cheques regalo...
                  </p>
                  <div class="miniatura" style="background-image:url([base_url]theme/theme/images/3cuadros/3.png); bottom:0"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--start doctor Section-->
    <section id="team" class="bg-silver-light">
      <div class="container pt-70 pb-70">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1 text-uppercase">Nuestros <span class="text-theme-colored2">Especialistas</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="[base_url]theme/theme/images/title-icon.png" alt="">
              </div>
              <p>Somos un equipo de profesionales a tu servicio. <br> Cada cliente es único y cada caso requiere una atención única.</p>
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-md-12">
              <div class="owl-carousel-4col">
                


                <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>11,'idioma'=>$_SESSION['lang']))->result() as $n=>$s):
                    $s->link = base_url('equipo/'.toUrl($s->id.'-'.$s->titulo));
                    $s->foto = base_url('img/blog/'.$s->foto);
                 ?>
                    <div class="item bg-white">
                      <div class="team-block maxwidth500 mb-sm-30">
                        <div class="team-upper-block">
                          <img class="img-fullwidth" src="<?= $s->foto ?>" alt="">
                          <ul class="styled-icons icon-bordered icon-circled icon-theme-colored pt-5">
                            <!-- 
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
 -->
                          </ul>
                        </div>
                        <div class="team-lower-block text-center border-bottom-2px border-theme-colored2 mt-0 pt-0 p-20">
                          <h3><a href="<?= $s->link ?>"><?= $s->titulo ?></a></h3>
                          <p><?= $s->descripcion_corta ?></p>
                        </div>
                      </div>
                    </div>
                <?php endforeach ?>



              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-theme-colored2-9" data-bg-img="[base_url]theme/theme/images/pages/1200x800-4.png" data-parallax-ratio="0.7">
      <div class="container pt-60 pb-60">
        
        <div class="section-title text-center mt-0">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 text-white">
              <h2 class="mt-0 line-height-1 text-uppercase">Atención al <span class="text-theme-colored2">Cliente</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="[base_url]theme/theme/images/title-icon3.png" alt="">
              </div>
              <p>En los últimos años experimentamos la presencia de un número importante de recién llegados, personas de todas partes que han decidido vivir cerca nuestro. Dentro de la línea que nos caracteriza de espíritu de servicio queremos ayudar a acercarnos para resolver las dudas en salud. Os atenderemos en estos idiomas:</p>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-offset-1 col-md-2 mb-md-50">
            <div class="funfact text-center pt-20" style="border:1px solid white">
              <img src="[base_url]img/catala.png" alt="">
              <h4 class="text-white text-uppercase font-weight-600">Catalán</h4>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 mb-md-50">
            <div class="funfact text-center pt-20" style="border:1px solid white">
              <img src="[base_url]img/espanol.png" alt="">
              <h4 class="text-white text-uppercase font-weight-600">Español</h4>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 mb-md-50">
            <div class="funfact text-center pt-20" style="border:1px solid white">
              <img src="[base_url]img/angles.png" alt="">
              <h4 class="text-white text-uppercase font-weight-600">Inglés</h4>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 mb-md-50">
            <div class="funfact text-center pt-20" style="border:1px solid white">
              <img src="[base_url]img/rus.png" alt="">
              <h4 class="text-white text-uppercase font-weight-600">Ruso</h4>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 mb-md-50">
            <div class="funfact text-center pt-20" style="border:1px solid white">
              <img src="[base_url]img/ucr.png" alt="">
              <h4 class="text-white text-uppercase font-weight-600">Ucraniano</h4>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="call-to-action pt-40 pb-40 mb-20 border-1px bg-lighter">
      <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
              <div class="icon-box icon-rounded-bordered left media mb-0 ml-60 ml-sm-0"> 
                <a class="media-left pull-left flip" href="#"> 
                  <i class="pe-7s-search text-theme-colored border-1px-theme-colored p-20"></i>
                </a>
                <div class="media-body">
                  <h3 class="media-heading heading">¿Necesitas ayuda?</h3>
                  <p>Le haremos la búsqueda del medicamento español homólogo al medicamento que se tomaba en su país de origen.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-4 text-center"> 
              <a href="[base_url]contacto.html" class="btn btn-dark btn-theme-colored btn-flat btn-xl mt-20">Pide información</a> 
            </div>
          </div>
      </div>
    </div>

    <!-- Divider: Services -->
    <section>
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-5 pt-40 pb-40">
            <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/sev.jpg" alt="">
          </div>
          <div class="col-md-7 pt-20" style="margin-left:0; margin-right:0">
            <div class="row mtli-row-clearfix">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>¿Qué <span class="text-theme-colored2"> ofrecemos?</span></h2>
                <div class="line-bottom"></div>
              </div>
              

              <?php $x = 0; foreach($this->elements->getServicios()->result() as $b): ?>
                <?php if($x==0): ?>
                  <div class="row" style="margin-left:0; margin-right:0">
                <?php endif; $x++; ?>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="icon-box left media p-0 mb-20"> 
                      <a class="media-left pull-left flip mr-20" href="<?= $b->link ?>">
                        <!--<i class="flaticon-medical-hospital36 text-theme-colored2 font-weight-600"></i>-->
                        <img src="<?= base_url('img/blog/'.$b->icono_main) ?>" alt="">
                      </a>
                      <div class="media-body">
                        <h4 class="media-heading heading mb-10"><?= $b->titulo ?></h4>
                        <p class="lh-15"><?= $b->descripcion_corta ?></p>
                      </div>
                    </div>
                  </div>
                <?php if($x==2): $x = 0; ?>
                </div>
                <?php endif ?>
              <?php endforeach; ?>

            </div>
          </div>
        </div>
      </div>
    </section>
    






<!-- Section: home -->
    <?php foreach($this->elements->getServicios(array('mostrar_en_main'=>1))->result() as $n=>$b): ?>
    <section id="home" class="divider parallax" data-bg-img="<?= base_url('img/blog/'.$b->banner_main) ?>">
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container pt-100 pb-100">
            <div class="row">
              <div class="<?= $n%2==0?'':'col-md-offset-8 col-md-4' ?> col-md-4 pt-15 pb-15" style="background: rgba(255,255,255,.8) !important">
                <div class="outline-border">
                  <h1 class="text-black-555 font-34 mt-0" style="line-height: 35px;"><?= $b->titulo_banner_main ?></h1>
                  <h5 class="text-black-555 font-14 font-weight-400 text-justify"><?= $b->texto_main ?></h5> 
                  <p class="text-right mb-0"> <a href="<?= $b->link ?>" class="btn btn-default btn-theme-colored btn-xs">Ir</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endforeach ?>






    <!--start gallary Section-->
    <section id="gallery" class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center mt-0">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1 text-uppercase">Nuestra <span class="text-theme-colored2">Galería</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="[base_url]theme/theme/images/title-icon.png" alt="">
              </div>
              <p>Una pequeña selección de imágenes de nuestra farmacia<br> y todo aquello que encontrarás!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              
              <!-- Portfolio Filter -->

              <div class="portfolio-filter text-center">
                <a href="#" class="active" data-filter="*">Todos</a>
                <?php foreach($this->db->get('galeria')->result() as $g): ?>
                  <a href="#gal<?= $g->id ?>" class="" data-filter=".gal<?= $g->id ?>"><?= $g->nombre_es ?></a>
                <?php endforeach ?>                
              </div>
              <!-- End Portfolio Filter -->
              
              <!-- Portfolio Gallery Grid -->
              <div class="gallery-isotope default-animation-effect grid-4 gutter-small clearfix" data-lightbox="gallery">
                
  

                <?php foreach($this->db->get('galeria_fotos')->result() as $g): ?>
                  <!-- Portfolio Item Start -->
                  <div class="gallery-item gal<?= $g->galeria_id ?>">
                    <div class="thumb">
                      <img class="img-fullwidth" src="<?= base_url('img/galeria/'.$g->foto) ?>" alt="project">
                      <div class="overlay-shade bg-theme-colored2"></div>
                      <div class="icons-holder">
                        <div class="icons-holder-inner">
                          <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                            <a href="<?= base_url('img/galeria/'.$g->foto) ?>" data-lightbox-gallery="gallery" title=""><i class="fa fa-picture-o"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Portfolio Item End -->
                <?php endforeach ?>
                
              </div>
              <!-- End Portfolio Gallery Grid -->
            </div>
          </div>
        </div>
      </div>
    </section>

     
    <!--start blog Section-->
    <section id="blog">
      <div class="container">
        <div class="section-title text-center mt-0">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1 text-uppercase">Últimas <span class="text-theme-colored2">Noticias</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="[base_url]theme/theme/images/title-icon.png" alt="">
              </div>
              <p>Sigue nuestro blog!<br>Te informaremos sobre todo tipo de novedades y artículos interesantes per ti</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            
            [foreach:blog]
                <div class="col-sm-6 col-md-4">
                  <article class="post clearfix maxwidth500 mb-sm-30">
                    <div class="entry-header">
                      <div class="post-thumb">
                        <img src="[foto]" alt="" class="img-responsive img-fullwidth">
                      </div>                    
                      <div class="entry-date media-left text-center flip bg-theme-colored2 pt-5 pr-15 pb-5 pl-15">
                        <ul>
                          <li class="font-16 text-white font-weight-600">[dia]</li>
                          <li class="font-12 text-white text-uppercase">[mes]</li>
                        </ul>
                      </div>
                    </div>
                    <div class="entry-content bg-white p-30">
                      <h3 class="entry-title mt-0">
                        <a href="[link]">[titulo]</a>
                      </h3>
                      <p>[texto]</p>
                      <div class="entry-meta pull-right flip mt-10">
                        <ul class="list-inline">
                          <!--<li><i class="fa fa-thumbs-o-up text-theme-colored2 mr-5"></i> 13</li>-->
                          <li><i class="fa fa-eye text-theme-colored2 mr-5"></i> [vistas]</li>
                        </ul>
                      </div>
                      <a class="btn btn-theme-colored mt-10 mb-0 pull-left flip" href="[link]">Leer más <i class="fa fa-angle-double-right"></i></a>
                      <div class="clearfix"></div>
                    </div>
                  </article>
                </div>
            [/foreach]
            
          </div>
        </div>
      </div>
    </section>


        
        

    <!--start Clients Section-->
  <section class="divider parallax layer-overlay overlay-theme-colored2-9" data-parallax-ratio="0.7">
      <div class="container pt-60 pb-60">       
        <div class="section-title text-center mt-0">
          <div class="row">
             <div class="col-md-8 col-md-offset-2 text-white">
              <h2 class="mt-0 line-height-1 text-uppercase">Colaboramos <span class="text-theme-colored2">con</span></h2>
              <img class="mb-10" src="[base_url]theme/theme/images/title-icon3.png" alt="">
              
            </div>
          </div>
          <div class="col-md-12" style="margin-top:20px;">
            <!-- Section: Clients -->
            <div class="owl-carousel-6col transparent text-center owl-nav-top">
              <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/sjd.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/aecc.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/amnistia.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/sigre.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/farmamundi.jpg" alt=""></a></div>
              <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/alcer.jpg" alt=""></a></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  [footer]

  

