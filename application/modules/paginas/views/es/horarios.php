[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="[base_url]theme/theme/images/pages/horarios.png">
      <div class="container pt-200 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">Horarios</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Nosotros</a></li>
                <li class="active text-theme-colored">Horarios</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

	<!-- Section: about -->
    <section id="about">
      <div class="container pb-0">
        <div class="row">
          <div class="col-md-4">
            <img src="[base_url]theme/theme/images/pages/roser.png" alt="">
          </div>
          <div class="col-md-4 mb-sm-30">
            <h1 class="mt-0">Dra. Roser Massana</h1>
            <ul class="text-black-999 mb-20">
              <li>Propietaria de Farmàcia Massana</li>
              <li>Farmacéutica</li>
            </ul>
            <p class="lead">"Bienvenido a la web de la nuestra farmacia. Estaremos encantados de ayudarte"</p>
            <h2><a href="tel:+34938034667" class="text-theme-colored2">93 803 46 67</a></h2>
            <p class="text-theme-colored2 mb-15"></p>
            <a href="[base_url]contacte.html" class="btn btn-dark btn-theme-colored2">Contactar</a>
          </div>
          <div class="col-md-4">
            <div class="border-10px p-30">
              <h5><i class="fa fa-clock-o text-theme-colored"></i> Horarios</h5>
              <div class="opening-hours text-left">
                <ul class="list-unstyled">
                  <li class="clearfix line-height-1"> <span> Dilluns - Dissabte </span>
                    <div class="value"> 8.00 - 21.00 </div>
                  </li>
                  <li class="clearfix line-height-1"> <span> Diumenge </span>
                    <div class="value"> Tancat</div>
                  </li>
                  <li class="clearfix line-height-1"> <span> Botiga online </span>
                    <div class="value"> 24 hores </div>
                  </li>
                </ul>
              </div>
              <h5 class="mt-30"><i class="fa fa-pencil-square-o text-theme-colored"></i> Estem avui de guàrdia?</h5>
              <p class="text-justify">Truca al telèfon 902 000 000 i podràs saber quina és la farmàcia de guardia.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Funfact -->
    <!-- 
<section class="divider parallax layer-overlay overlay-theme-colored2-9" data-bg-img="[base_url]theme/theme/images/pages/1200x800.png" data-parallax-ratio="0.7">
      <div class="container pt-60 pb-60">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="1754" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Happy Patients</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-rocket mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="675" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Our Services</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-add-user mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="248" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Our Doctors</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-global mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="24" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Service Points</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
 -->

    <!-- Section: Departments -->
    <!-- 
<section id="depertments" class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1 text-uppercase">Clinic <span class="text-theme-colored2">Departments</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-centent">
          <div class="row">
            <div class="col-md-12">
              <div class="horizontal-tab-centered">
                <div class="text-center">
                  <ul class="nav nav-pills mb-10">
                    <li class="active"> <a href="#tab11" class="" data-toggle="tab" aria-expanded="false"> Orthopaedics</a> </li>
                    <li class=""> <a href="#tab12" data-toggle="tab" aria-expanded="false"> Cardiology</a> </li>
                    <li class=""> <a href="#tab13" data-toggle="tab" aria-expanded="true"> Neurology</a> </li>
                    <li class=""> <a href="#tab14" data-toggle="tab" aria-expanded="false"> Dental</a> </li>
                    <li class=""> <a href="#tab15" data-toggle="tab" aria-expanded="false"> Haematology</a> </li>
                  </ul>
                </div>
                <div class="tab-content bg-white">
                  <div class="tab-pane fade in active" id="tab11">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Orthopaedic</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-6.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab12">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Cardiology</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-7.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab13">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Neurology</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-8.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab14">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Dental</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-9.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab15">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Hemtology</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-10.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </section>
 -->

    <!--start Clients Section-->
    <!-- 
<section class="clients bg-theme-colored2">
      <div class="container pt-10 pb-10">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!~~ Section: Clients ~~>
              <div class="owl-carousel-6col transparent text-center owl-nav-top">
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-1.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-2.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-3.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-4.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-5.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-6.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-7.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-8.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-9.png" alt=""></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
 -->

	

</div>
<!-- End main-content -->
[footer]