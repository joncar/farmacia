[menu]
<!-- Start main-content -->
<div class="main-content">

    <!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/equipo.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/equipo.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Equipo
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	

	<!--start doctor Section-->
    <section id="team" class="bg-silver-light">
      <div class="container pt-70 pb-70">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1 text-uppercase">Nuestros <span class="text-theme-colored2">Especialistas</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="[base_url]theme/theme/images/title-icon.png" alt="">
              </div>
              <p>Somos un equipo de profesionales a tu servicio. <br> Cada cliente es único y cada caso requiere una atención única.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <?php $this->db->order_by('orden','ASC'); ?>
          <?php foreach($equipo->result() as $n=>$s): ?>
            <div class="col-md-4 pt-30 pb-30">
              <div class="box-hover-effect effect-alaska">
                  <div class="effect-wrapper">
                    <div class="thumb">
                      <img class="img-fullwidth" src="<?= $s->foto ?>" alt="project">
                    </div>
                    <div class="bottom-info-box bg-theme-colored-transparent-9">
                      <div class="title">                        
                        <h3 class="title text-white mt-0 mb-0"><?= $s->titulo ?></h3>
                        <a class="text-white" href="<?= $s->link ?>"><?= $s->descripcion_corta ?></a>
                        <div class="invisible-parts text-white mt-20">
                          <p><?= $s->texto ?></p>
                          <a href="<?= $s->link ?>" class="btn btn-dark btn-theme-colored2">Ver más</a>
                        </div>                        
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    </section>

    <!-- Section: Features -->
    <section>
      <div class="container-fluid pt-0 pb-0">
        <div class="row equal-height">
          <div class="col-md-6 bg-theme-colored2 sm-height-auto">
            <div class="p-50 p-sm-0 p-sm-0 pt-sm-30 pb-sm-30">
              <h2 class="text-white">Cada cliente es único y necesita una solución personalizada</h2>
              <p class="text-white">Nuestros especialistas estarán encantados de atenderte e informarte sobre tus opciones. Si quieres realizar una consulta antes puedes rellenar este formulario y te responderemos lo más rápido posible.</p>
              <!-- <a href="page-services-engine.html" class="btn btn-default btn-circled mt-10">view details</a> -->
            </div>
          </div>
          <div class="col-md-6 bg-img-cover sm-height-auto" data-bg-img="[base_url]theme/theme/images/pages/530x450.png">
            <div class="p-50 p-sm-0 p-sm-0 pt-sm-30 pb-sm-30">
              <!-- Reservation Form Start-->
              <form name="contact_form" class="" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#response')" method="post">
                <h3 class="mt-0 line-bottom mb-30">
                  Formulario <span class="text-theme-colored font-weight-600" style="color:#fff !important">de contacto</span>
                </h3>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group mb-30">
                      <input placeholder="Nombre y Apellidos" type="text" id="reservation_name" name="nombre" required="" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-30">
                      <input placeholder="Email" type="text" id="reservation_email" name="email" class="form-control" required="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-30">
                      <input placeholder="Teléfono" type="text" name="extras[telefono]" class="form-control" required="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-30">
                      <div class="styled-select">
                        <select name="extras[especialidad]" class="form-control" required="">
                          <option value="">- Escoge la especialidad -</option>
                          <option value="Orthopaedics">Medicamentos</option>
                          <option value="Cardiology">Homeopatía</option>
                          <option value="Neurology">Fórmulas magistrales</option>
                          <option value="Dental">Productos naturales</option>
                          <option value="Haematology">Analíticas i pruebas</option>
                          <option value="Blood Test">Preparación medicación</option>
                          <option value="Emergency Care">Deshabituación tabáquica</option>
                          <option value="Outdoor Checkup">Ortopedia</option>
                          <option value="Cancer Service">Preparación botiquín</option>
                          
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-30">
                      <input name="extras[fecha_reserva]" class="form-control required date-picker" type="text" placeholder="Fecha" aria-required="true">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="checkbox">
                      <label style="color:#fff">
                        <input type="checkbox" name="politicas" value="1"> He leído y acepto las <a href="<?= base_url() ?>aviso-legal.html" target="_new" style="text-decoration: underline;color:#fff"><u>políticas de privacidad* </u></a>
                      </label>
                    </div>
                    <div id="response"></div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group mb-0 mt-0">
                      <input name="form_botcheck" class="form-control" type="hidden" value="">
                      <button type="submit" class="btn btn-colored btn-theme-colored btn-lg btn-flat border-left-theme-color-2-4px" data-loading-text="Please wait...">Enviar</button>
                    </div>
                  </div>
                </div>
              </form>
              <!-- Reservation Form End-->

              <!-- Reservation Form Validation Start-->
              <script type="text/javascript">
                $("#reservation_form").validate({
                  submitHandler: function(form) {
                    var form_btn = $(form).find('button[type="submit"]');
                    var form_result_div = '#form-result';
                    $(form_result_div).remove();
                    form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                    var form_btn_old_msg = form_btn.html();
                    form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                    $(form).ajaxSubmit({
                      dataType:  'json',
                      success: function(data) {
                        if( data.status == 'true' ) {
                          $(form).find('.form-control').val('');
                        }
                        form_btn.prop('disabled', false).html(form_btn_old_msg);
                        $(form_result_div).html(data.message).fadeIn('slow');
                        setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                      }
                    });
                  }
                });
              </script>
              <!-- Reservation Form Validation Start -->
            </div>
          </div>
        </div>
      </div>
    </section>    

    <section>
      <img src="[base_url]img/equipoFooter.png" alt="" style="width:100%;">
    </section>
</div>
<!-- End main-content -->
[footer]