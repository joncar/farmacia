
<div class="body-overlay"></div>
<div id="side-panel" class="dark layer-overlay overlay-white-8" style="background-image: url(<?= base_url() ?>theme/theme/images/bg/bg1.jpg)">
  <div class="side-panel-wrap">
    <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon_close font-30"></i></a></div>
    <div class="side-panel-widget">
      <div class="widget">
        <a href="javascript:void(0)"><img alt="logo" src="<?= base_url() ?>theme/theme/images/logo-wide.png"></a>
        <p class="mt-20">Tu farmacia de confianza. con más de 50 años de experiencia en farmacia, en Vilanova del Camí.</p>
      </div>
      <div class="widget">
        <h5 class="widget-title">Enlaces de interés</h5>
        <nav>
          <ul class="nav nav-list">
            <li><a href="<?= base_url() ?>">Inicio</a></li>
            <li><a href="<?= base_url() ?>equipo">Equipo</a></li>
            <li><a class="tree-toggler nav-header">Qué ofrecemos <i class="fa fa-angle-down"></i></a>
              <ul class="nav nav-list tree">

              <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>9,'idioma'=>$_SESSION['lang']))->result() as $b): ?>
                <li><a href="<?= base_url('servicios/'.toUrl($b->id.'-'.$b->titulo)) ?>"><?= $b->titulo ?></a></li>
              <?php endforeach; ?>
              </ul>
            </li>
            <li><a href="<?= base_url() ?>contacto.html">Contacto</a></li>
          </ul>
        </nav>
      </div>
      <div class="widget">
        <h5 class="widget-title">Horarios</h5>
        <div class="opening-hours text-left">
          <ul class="list-unstyled">
            <li class="clearfix line-height-1"> <span> Lun - Vie </span>
              <div class="value"> 8.30 - 14h / 16 - 20:30h </div>
            </li>
            <li class="clearfix line-height-1"> <span> Sabados </span>
              <div class="value"> 9 - 14h / 17 - 20:30h </div>
            </li>
            <li class="clearfix line-height-1"> <span> Domingos </span>
              <div class="value"> 10 - 12h </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="widget">
        <h5 class="widget-title">Información de contacto</h5>
        <ul>
          <li class="font-14 mb-5"><i class="fa fa-phone text-theme-colored"></i> <a href="tel:938034667" class="text-gray">93 803 46 67</a> </li>
          <li class="font-14 mb-5"><i class="fa fa-globe text-theme-colored"></i> <a class="text-gray" href="<?= base_url() ?>">www.farmaciamassana.cat</a></li>
          <li class="font-14 mb-5"><i class="fa fa-envelope-o text-theme-colored"></i> <a href="mailto:info@farmaciamassana.cat" class="text-gray">farmacia@farmaciamassana.cat</a> </li>
        </ul>
      </div>
      <div class="widget">
        <ul class="styled-icons icon-dark icon-theme-colored icon-sm">
         
          <li><a href="https://www.facebook.com/farmaciamassana/"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.instagram.com/farmaciamassana/"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <p>Copyright &copy;<?= date("Y") ?> Jordi Magaña</p>
    </div>
  </div>
</div