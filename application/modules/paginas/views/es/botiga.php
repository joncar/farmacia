[menu]
<!-- Start main-content -->
<div class="main-content">

    <!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/stop2go.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/stop2go.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Tienda
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-9">
              <?php echo $output ?>
            </div>
            <div class="col-md-3">
              <div class="sidebar sidebar-right mt-sm-30">
                <div class="widget">
                  <h5 class="widget-title">Buscar producto</h5>
                  <div class="search-form">
                    <form>
                      <div class="input-group">
                        <input type="text" placeholder="Click to Search" class="form-control search-input">
                        <span class="input-group-btn">
                        <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="widget">
                  <h4 class="widget-title">Categorías</h4>
                  <div class="categories">
                    <ul class="list list-border angle-double-right">
                      <li><a href="[base_url]stop2go.html">Creative<span>(19)</span></a></li>
                      <li><a href="[base_url]stop2go.html">Portfolio<span>(21)</span></a></li>
                      <li><a href="[base_url]stop2go.html">Fitness<span>(15)</span></a></li>
                      <li><a href="[base_url]stop2go.html">Gym<span>(35)</span></a></li>
                      <li><a href="[base_url]stop2go.html">Personal<span>(16)</span></a></li>
                    </ul>
                  </div>
                </div>
                
                <div class="widget">
                  <h4 class="widget-title">Ofertas</h4>
                  <div class="box-hover-effect effect-florida mb-30">
                      <div class="effect-wrapper">
                        <div class="thumb">
                          <img class="img-fullwidth" src="[base_url]img/tienda/promo10.jpg" alt="project">
                        </div>
                        <div class="info-box">
                          <div class="info-title bg-theme-colored-transparent-9">
                            <p class="title text-white mt-0">Title place here</p>
                          </div>
                          <div class="info-content text-white bg-theme-colored-transparent-9">
                            <p class="text-white mt-0"><b>Title place here</b></p>
                            <p style="font-size: 12px">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a class="btn btn-dark" href="[base_url]producto.html">Veure mès</a>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="box-hover-effect effect-florida mb-30">
                      <div class="effect-wrapper">
                        <div class="thumb">
                          <img class="img-fullwidth" src="[base_url]img/tienda/promo11.jpg" alt="project">
                        </div>
                        <div class="info-box">
                          <div class="info-title bg-theme-colored-transparent-9">
                            <p class="title text-white mt-0">Title place here</p>
                          </div>
                          <div class="info-content text-white bg-theme-colored-transparent-9">
                            <p class="text-white mt-0"><b>Title place here</b></p>
                            <p style="font-size: 12px">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a class="btn btn-dark" href="[base_url]producto.html">Veure mès</a>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="box-hover-effect effect-florida mb-30">
                      <div class="effect-wrapper">
                        <div class="thumb">
                          <img class="img-fullwidth" src="[base_url]img/tienda/promo12.jpg" alt="project">
                        </div>
                        <div class="info-box">
                          <div class="info-title bg-theme-colored-transparent-9">
                            <p class="title text-white mt-0">Title place here</p>
                          </div>
                          <div class="info-content text-white bg-theme-colored-transparent-9">
                            <p class="text-white mt-0"><b>Title place here</b></p>
                            <p style="font-size: 12px">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a class="btn btn-dark" href="[base_url]producto.html">Veure mès</a>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>

                <div class="widget">
                  <h4 class="widget-title">Novedades</h4>
                  <div class="latest-posts">
                    <article class="post media-post clearfix pb-0 mb-10">
                      <a class="post-thumb" href="#"><img src="[base_url]img/tienda/promo13.jpg" alt=""></a>
                      <div class="post-right">
                        <h5 class="post-title mt-0"><a href="[base_url]producto.html">Sustainable Construction</a></h5>
                        <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                      </div>
                    </article>
                    <article class="post media-post clearfix pb-0 mb-10">
                      <a class="post-thumb" href="#"><img src="[base_url]img/tienda/promo14.jpg" alt=""></a>
                      <div class="post-right">
                        <h5 class="post-title mt-0"><a href="[base_url]producto.html">Industrial Coatings</a></h5>
                        <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                      </div>
                    </article>
                    <article class="post media-post clearfix pb-0 mb-10">
                      <a class="post-thumb" href="#"><img src="[base_url]img/tienda/promo15.jpg" alt=""></a>
                      <div class="post-right">
                        <h5 class="post-title mt-0"><a href="[base_url]producto.html">Storefront Installations</a></h5>
                        <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
	

</div>
<!-- End main-content -->
[footer]