[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="[base_url]theme/theme/images/pages/servicios-detalles.png">
      <div class="container pt-200 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">Realizar pedido</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Stop&Go</a></li>
             
                <li class="active text-theme-colored">Realizar pedido de Stop&Go</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

		<!-- Section: Service details -->
	    <section>
	      <div class="container">
	        <div class="section-content">
	          <div class="row">
	            <div class="col-xs-12 col-sm-8 col-md-8 pull-left flip">	              
	              <div class="row">
	                <div class="col-md-12">
	                 	


						<div class="megamenu megamenu-bg-img" style="background-image: url('[base_url]theme/theme/images/pages/megamenu-dropdown.jpg');background-position: -27px bottom;background-repeat: no-repeat; border-radius: 0 0 6px 6px">
				          <div class="row ml-20 mr-20 mt-20">
				            <!-- Titulos --->
				            <div class="col-md-12 mb-20 mt-20 hidden-sm hidden-xs">
				              <h2 class="megamenu-col-title">¿Cómo funciona Stop&go?</h2> 
				            </div>
				            <div class="col-md-3 ml-0">
				              <h4 class="megamenu-col-title">Realiza tu pedido por los diferentes canales</h4>                      
				            </div>
				            <div class="col-md-3">
				              <h4 class="megamenu-col-title">Atendemos tu pedido al momento</h4>                      
				            </div>
				            <div class="col-md-3">
				              <h4 class="megamenu-col-title">Preparamos tu pedido en menos de 24 horas</h4>                      
				            </div>
				            <div class="col-md-3">
				              <h4 class="megamenu-col-title">Te avisamos y puedes recoger tu pedido</h4>                      
				            </div>
				            <!-- End titulos ---->
				            <div class="col12 ml-0 mb-20">
				              <img src="<?= base_url('img/megamenu.png') ?>" alt="" style="width:100%;">
				            </div>
				            <!--- Textos ---->
				            <div class="col-md-3 ml-0">                      
				              <p>Por teléfono en el 93 803 46 67,<br>por whatsapp en el 696 841 650 <br>o por email <a href="mailto:farmacia@farmaciamassana.cat" class="text-theme-colored">AQUÍ</a></p>
				            </div>
				            <div class="col-md-3">                      
				              <p>Recibirás una respuesta de que estamos preparando tu pedido.</p>
				            </div>
				            <div class="col-md-3">                      
				              <p>En la fecha y hora pactada tendrás tu pedido preparado.</p>
				            </div>
				            <div class="col-md-3">                      
				              <p>Cuando llegues te priorizaremos el turno y así sólo tendrás que pagar.</p>
				            </div>
				            
				            
				           
				            <!--- End textos ---->
				          </div>
				        </div>



	                </div>	               
	              </div>
	            </div>
	            <div class="col-sx-12 col-sm-4 col-md-4 sidebar pull-right flip">	              	              				
	              	<div class="widget">
						<div class="border-1px p-20 mb-0" style="background: #f8f8f8;">
						  <h3 class="text-theme-colored mt-0 pt-5">Realizar pedido</h3>
						  <hr>    

						    <div class="form-group" style="font-size:14px;">
						      <div>Nombre y Apellidos <small>*</small></div>
						      <input name="nombre" type="text" placeholder="" class="form-control">
						    </div>
						    <div class="form-group" style="font-size:14px;">
						      <div>Teléfono <small>*</small></div>
						      <input name="telefono" type="text" placeholder="" class="form-control">
						    </div>
						    <div class="form-group" style="font-size:14px;">
						      <div>Email <small>*</small></div>
						      <input name="email" class="form-control email" type="email" placeholder="">
						    </div>
						    

						    <div class="form-group" style="font-size:14px;">
						      <div>Recogida de Producto</div>      
						    </div>
						    <div class="row">
						      <div class="col-sm-6">
						          <div class="form-group" style="font-size:14px;">
						            <div>Fecha <small>*</small></div>
						            <input name="fecha" class="form-control date-picker" type="text" placeholder="">
						          </div>
						       </div>    
						       <div class="col-sm-6">
						          <div class="form-group" style="font-size:14px;">
						            <div>Hora <small>*</small></div>
						            <input name="hora" class="form-control time-picker" type="text" placeholder="">
						          </div>
						       </div>
						    </div>
						    <div class="form-group" style="font-size:14px;">
						      <div>Comentarios o Sugerimientos <small>*</small></div>
						      <textarea name="comentarios" class="form-control required" rows="5" placeholder=""></textarea>
						    </div>
						    <div class="form-group" style="font-size:14px;">
						        <div class="checkbox pl-20">
						    	    <div style="font-size:12px; line-height:14px;">
						    	      <input type="checkbox" name="aviso" value="1"> Acepto recibir información comercial, también por correo electrónico.
						    	    </div>
						    	</div>
						    	<div class="checkbox pl-20">
						    	    <div style="font-size:12px; line-height:14px;">
						    	      <input type="checkbox" name="politica" value="1"> He leído y acepto los <a href="#" class="text-theme-colored">Términos y condiciones de contratación</a> y la <a href="#" class="text-theme-colored">Política de Privacidad</a>
						    	    </div>
						    	</div>
						    </div>
						    <div class="form-group">						    
						      <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Enviar</button>
						    </div>
						</div>
	              	</div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </section>	    
	  </div>
	  <!-- end main-content -->  

	

</div>
<!-- End main-content -->
[footer]