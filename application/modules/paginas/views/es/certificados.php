[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="[base_url]theme/theme/images/pages/certificados.png">
      <div class="container pt-200 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">Certificats</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="#">Inici</a></li>
                <li><a href="#">Nosaltres</a></li>
                <li class="active text-theme-colored">Certificats</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

	

	<!-- Divider: Services -->
    <section class="bg-silver-light">
      <div class="container pt-30 pb-0">
        <div class="row">
          <div class="col-md-5">
            <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/lli.png" alt="">
          </div>
          <div class="col-md-7 pt-20">
            <div class="row mtli-row-clearfix">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Llicenciats <span class="text-theme-colored2">en...</span></h2>
                <div class="line-bottom"></div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="icon-box left media p-0 mb-40"> 
                  <a class="media-left pull-left flip mr-20" href="#"><i class="flaticon-medical-hospital36 text-theme-colored2 font-weight-600"></i></a>
                  <div class="media-body">
                    <h4 class="media-heading heading mb-10">Farmàcia</h4>
                    <p>Som professionals amb llicenciatures pròpies.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="icon-box left media p-0 mb-40"> 
                  <a class="media-left pull-left flip mr-20" href="#"><i class="flaticon-medical-illness text-theme-colored2 font-weight-600"></i></a>
                  <div class="media-body">
                    <h4 class="media-heading heading mb-10">Òptica i audició</h4>
                    <p>Som professionals amb llicenciatures pròpies.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="icon-box left media p-0 mb-40"> 
                  <a class="media-left pull-left flip mr-20" href="#"><i class="flaticon-medical-stethoscope10 text-theme-colored2 font-weight-600"></i></a>
                  <div class="media-body">
                    <h4 class="media-heading heading mb-10">Dietes i nutricionisme</h4>
                    <p>Som professionals amb llicenciatures pròpies.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="icon-box left media p-0 mb-40"> 
                  <a class="media-left pull-left flip mr-20" href="#"><i class="flaticon-medical-hospital35 text-theme-colored2 font-weight-600"></i></a>
                  <div class="media-body">
                    <h4 class="media-heading heading mb-10">Homeopatia</h4>
                    <p>Som professionals amb llicenciatures pròpies.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="icon-box left media p-0 mb-40"> 
                  <a class="media-left pull-left flip mr-20" href="#"><i class="flaticon-medical-medical44 text-theme-colored2 font-weight-600"></i></a>
                  <div class="media-body">
                    <h4 class="media-heading heading mb-10">Infermeria</h4>
                    <p>Som professionals amb llicenciatures pròpies.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="icon-box left media p-0 mb-40"> 
                  <a class="media-left pull-left flip mr-20" href="#"><i class="flaticon-medical-tablets9 text-theme-colored2 font-weight-600"></i></a>
                  <div class="media-body">
                    <h4 class="media-heading heading mb-10">Química</h4>
                    <p>Som professionals amb llicenciatures pròpies.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Certificates -->
    <section class="divider parallax layer-overlay overlay-theme-colored2-8" data-bg-img="[base_url]theme/theme/images/pages/1200x800-1.png" data-parallax-ratio="0.7">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 text-center">
            <a href="images/certificates/b1.jpg" data-lightbox-gallery="certificates" title="Certificate"><img src="[base_url]theme/theme/images/pages/dip1.png" alt=""></a>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 text-center">
            <a href="images/certificates/b2.jpg" data-lightbox-gallery="certificates" title="Certificate"><img src="[base_url]theme/theme/images/pages/dip2.png" alt=""></a>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 text-center">
            <a href="images/certificates/b3.jpg" data-lightbox-gallery="certificates" title="Certificate"><img src="[base_url]theme/theme/images/pages/dip3.png" alt=""></a>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 text-center">
            <a href="images/certificates/b4.jpg" data-lightbox-gallery="certificates" title="Certificate"><img src="[base_url]theme/theme/images/pages/dip4.png" alt=""></a>
          </div>
        </div>
      </div>
    </section>

</div>
<!-- End main-content -->
[footer]