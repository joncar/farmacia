[menu]
<!-- Start main-content -->
<div class="main-content">

    <!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/historia.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/historia.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Historia <span class="text-theme-colored">y Valores</span>
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	

	<!-- Section: About -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <img class="pull-left flip mr-15 thumbnail" src="[base_url]theme/theme/images/pages/rb.png" alt="">
            <p><b>Todo empieza el año 1962,</b> cuando Josep Mª Massana barcelonés, licenciado en Farmacia, Farmacología, Hematología y con una inquietud constante por la física y la astronomía, decide establecerse en Vilanova del Camí que, en aquella época, tenía poco más de 1.000 habitantes, y donde las calles estaban aún por asfaltar.
<br>Se instaló en un local situado en la Plaza Mayor, acondicionándolo con un tablero de madera y un pequeño laboratorio para poder elaborar fórmulas magistrales.
<br>El servicio de envío diario de los medicamentos desde Barcelona a Vilanova se hacía a través de los ferrocarriles de la Generalitat, donde el maquinista se hacía responsable de guardar la caja de los remedios durante todo el trayecto.
<br><br><br><b>En 1977,</b> la farmacia se traslada a la Calle Mayor. A partir de entonces, gracias a los avances científicos, el abanico de moléculas terapéuticas se amplió de una manera espectacular para poder hacer frente a las diferentes patologías.
<br> De esta manera, los vilanovenses podían tener los medicamentos con rapidez profesional y empezar muy pronto la terapia prescrita por los especialistas asegurando así la remisión de sus dolencias.
<br>Con los años la formación ha estado, es y será continua en todos los sentidos, tanto a nivel de conocimientos de salud como tecnológicos.
<br>Nuestro equipo humano, formado por farmacéuticas licenciadas en la facultad de Barcelona y técnicos formados en la escuela Milà y Fontanals, actualiza los conocimientos (formulaciones magistrales, farmacología, homeopatía, plantas medicinales...) para aplicarlos a la prevención y promoción de la salud.
</p>
<div class="clearfix"></div>
            
            <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
            <div class="row">
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/1.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/2.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/3.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/4.png" class="img-fullwidth"> </a> </div>
            </div>
   <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>         
  <!-- Section: about -->
    <section id="about">
      <div class="container pb-0">
        <div class="row">
          <div class="col-md-8 wow fadeInLeft animation-delay1">
            <h2 class="mt-0">Nuestros<span class="text-theme-colored2"> valores</span></h2>
            <h3 class="mt-0">La constancia y nuestros valores nos hacen ser mejores</h3>
            <p>Nuestro trabajo es nuestra pasión y queremos transmitir esta pasión a nuestros clientes. Queremos que los clientes se sientan apoyados, informados y satisfechos. Si lo conseguimos es nuestro gran éxito!</p>
            <div class="row mt-20 mb-sm-30">
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Colaboraciones</h4>
                    <p class="">Trabajamos de forma conjunta con nuestros clientes y proveedores</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Confianza</h4>
                    <p class="">El respeto, la integridad y la sinceridad nos llevan por el camino correcto</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Servicios</h4>
                    <p class="">Nuestro personal y nuestros clientes nos inspiran compromiso y pasión</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Dedicación</h4>
                    <p class="">Trabajamos con rigor y simplicidad para conseguir todo lo que nos proponemos</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 wow fadeInRight animation-delay4">
            <img src="[base_url]theme/theme/images/pages/xx.jpg" alt="">
          </div>
        </div>
      </div>
    </section>
    
            <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
            <h2 class="mt-0">Cómo lo<span class="text-theme-colored2"> queremos hacer?</span></h2>
                        <p>Con el constante instinto de cuidaros, hacemos de vuestra salud nuestra prioridad, ponemos a vuestro servicio todo nuestro conocimiento y experiencia profesional, porque mejorar vuestra calidad de vida sea nuestro sincero compromiso, implicándonos en la prevención y deseando daros las soluciones más adecuadas.
 <br><br> Tenemos el reto de mantener vuestra confianza y el deber de respetar el medio ambiente sin perder la esperanza de conseguir una sociedad más humana y solidaria.

</p>
          </div>
          <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
            <div class="row" style="margin-left:0; margin-right:0">
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/5.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/6.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/7.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/8.png" class="img-fullwidth"> </a> </div>
            </div>
            <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
        </div>
      </div>
    </section>
  </div>

    <!-- Divider: Funfact -->
    <!-- 
<section class="divider parallax layer-overlay overlay-theme-colored2-9" data-bg-img="[base_url]theme/theme/images/pages/1200x800-4.png" data-parallax-ratio="0.7">
      <div class="container pt-60 pb-60">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="1754" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Clientes satisfechos</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-rocket mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="675" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Productos</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-add-user mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="248" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Ventas online</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-global mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="24" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Horas a tu servicio</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
 -->

  <!-- end main-content -->

</div>
<!-- End main-content -->
[footer]