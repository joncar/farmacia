<div class="border-1px p-20 mb-0" style="background: #f8f8f8;">
  <h3 class="text-theme-colored mt-0 pt-5">Solicitud de elaboración de medicamento</h3>
  <hr>    

    <div class="form-group" style="font-size:14px;">
      <div>Nombre y Apellidos <small>*</small></div>
      <input name="nombre" type="text" placeholder="" class="form-control">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Teléfono <small>*</small></div>
      <input name="telefono" type="text" placeholder="" class="form-control">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Email <small>*</small></div>
      <input name="email" class="form-control email" type="email" placeholder="">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>C.P <small>*</small></div>
      <input name="cp" class="form-control email" type="text" placeholder="">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Dirección completa <small>*</small></div>
      <input name="direccion" class="form-control email" type="text" placeholder="">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Ciudad <small>*</small></div>
      <input name="ciudad" class="form-control email" type="text" placeholder="">
    </div>

    <div class="form-group" style="font-size:14px;">
      <div>Provincia <small>*</small></div>
      <input name="provincia" class="form-control email" type="text" placeholder="">
    </div>

    <div class="form-group" style="font-size:14px;">
      <div class="mb-15">Adjuntar receta médica en PDF o JPG</div>
      <?= $input_fields['receta1']->input ?>      
    </div>

    <div class="form-group" style="font-size:14px;">      
      <?= $input_fields['receta2']->input ?>      
    </div>

    <div class="form-group" style="font-size:14px;">      
      <?= $input_fields['receta3']->input ?>      
    </div>

    <!--<div class="form-group" style="font-size:14px;">
      <div>Recogida de Producto</div>      
    </div>
    <div class="row">
      <div class="col-sm-6">
          <div class="form-group" style="font-size:14px;">
            <div>Fecha <small>*</small></div>
            <input name="fecha" class="form-control date-picker" type="text" placeholder="">
          </div>
       </div>    
       <div class="col-sm-6">
          <div class="form-group" style="font-size:14px;">
            <div>Hora <small>*</small></div>
            <input name="hora" class="form-control time-picker" type="text" placeholder="">
          </div>
       </div>
    </div>-->
    
    <div class="form-group" style="font-size:14px;">
      <div>Comentarios o Sugerencias <small>*</small></div>
      <textarea name="comentarios" class="form-control required" rows="5" placeholder=""></textarea>
    </div>
  
    <div class="form-group" style="font-size:14px;">
        <div class="checkbox pl-20">
    	    <div style="font-size:12px; line-height:14px;">
    	      <input type="checkbox" name="aviso" value="1"> Acepto recibir información comercial, también por correo electrónico.
    	    </div>
    	</div>
    	<div class="checkbox pl-20">
    	    <div style="font-size:12px; line-height:14px;">
    	      <input type="checkbox" name="politica" value="1"> He leído y acepto los <a href="#" class="text-theme-colored">Términos y condiciones de contratación</a> y la <a href="#" class="text-theme-colored">Política de Privacidad</a>
    	    </div>
    	</div>
    </div>

  <div id='report-error' style="display:none" class='alert alert-danger'></div>
  <div id='report-success' style="display:none" class='alert alert-success'></div>
    
    <div class="form-group">
      <?php
        foreach($hidden_fields as $hidden_field){
                echo $hidden_field->input;
        }
      ?>
      <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Enviar</button>
    </div>
</div>