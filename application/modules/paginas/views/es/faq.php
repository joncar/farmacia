[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?= base_url() ?>theme/theme/images/pages/faq.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="<?= base_url() ?>theme/theme/images/pages/faq.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  FAQ
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	
    <section class="position-inherit">
      <div class="container">
        <div class="row">
          <div class="col-md-3 scrolltofixed-container">
            <div class="list-group scrolltofixed z-index-0">
              <a href="#section-one" class="list-group-item smooth-scroll-to-target"> ¿Cómo funciona la targeta cliente? 
?</a>

              <a href="#section-two" class="list-group-item smooth-scroll-to-target">¿Qué es Stop and go?</a>

              <a href="#section-three" class="list-group-item smooth-scroll-to-target">¿Qué tengo que hacer si quiero un jarabe? unas cápsulas? una crema?...</a>

              <a href="#section-four" class="list-group-item smooth-scroll-to-target">¿Qué tengo que hacer cuando quiera hacer un test analítico?
</a>

              <a href="#section-five" class="list-group-item smooth-scroll-to-target">¿Te han realizado una intervención y necesitas temporalmente unas muletas? ¿Una silla de ruedas?
</a>
            </div>
          </div>
          <div class="col-md-9">
            <div id="section-one" class="mb-50">
              <h3>¿Cómo funciona la tarjeta cliente?</h3>
              <hr>
              <p class="mb-20">Acumula el importe de las compras en EFP y parafarmacia, cuando llegues a 200€ de compra te mandaremos un cheque de 6€ de regalo que lo podrás descambiar en la próxima compra de productos de parafarmacia y EFP en cualquiera de las 647 farmacias adheridas a Xarxafarma de todo el país. Para consular tus compras podrás hacerlo on line a través de tu cuenta cliente o bajándote la App de Xarxafarma.
            </div>
            <div id="section-two" class="mb-50">
              <h3>¿Qué es Stop and go?</h3>
              <hr>
              <p class="mb-20">Te lo ponemos muy fácil.<br>
Nos llamas al 938034667 o nos mandas un mensaje de whatsapp al 696841650 diciendo lo que te interesa y a qué hora pasará a recogerlo. Nosotros te prepararemos tu pedido para que cuando llegues a la farmacia te priorizaremos el turno y así sólo tendrás que pagar.
<br>Quan vinguis a recollir un encàrrec, no et preocupis et garantim que serà aparcar i marxar.</p>
              
            </div>
            <div id="section-three" class="mb-50">
              <h3>¿Qué tengo que hacer si quiero un jarabe? unas cápsulas? una crema?...
</h3>
              <hr>
              <p class="mb-20">Rellena el formulario del apartado de fórmulas de nuestra web, nos mandas una fotografía de la petición de tu prescriptor por whatsapp o nos la traes a la farmacia y te la prepararemos.
</p>
            </div>
            <div id="section-four" class="mb-50">
              <h3>¿Qué tengo que hacer cuando quiera hacer un test analítico?</h3>
              <hr>
              <p class="mb-20">No es necesario que pidas hora. El día que te vaya bien hacerte la prueba no desayunes y pasa por la farmacia. Sólo necesitas una muestra muy pequeña de tu sangre que te la sacaremos de tu dedo. Depenendiendo del test necesitaremos un tiempo u otro para poder darte el resultado pero no serán más de 24h.</p>
            </div>
            <div id="section-five" class="mb-50">
              <h3>¿Te han realizado una intervención y necesitas temporalmente unas muletas? ¿Una silla de ruedas?</h3>
              <hr>
              <p class="mb-20">Te lo solucionamos. En la farmacia alquilamos muletas y sillas de ruedas para unos días. Si nos llamas te informaremos.
</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	

</div>
<!-- End main-content -->
[footer]