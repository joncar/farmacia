[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="[base_url]theme/theme/images/pages/producto.png">
      <div class="container pt-200 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">Producto</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Stop&Go</a></li>
                <li class="active text-theme-colored">Producto</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

	
<section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="product">
              <div class="col-md-5">
                <div class="product-image">
                  <ul class="owl-carousel-1col" data-nav="true">
                  <li data-thumb="[base_url]img/productos/">
                    <a href="<?= base_url('img/productos/'.$d->foto) ?>" data-lightbox="single-product">
                      <img src="<?= base_url('img/productos/'.$d->foto) ?>" alt="">
                    </a>
                  </li>
                </ul>
                </div>
              </div>
              <div class="col-md-7">
                <div class="product-summary">
                  <h2 class="product-title"><?= $d->nombre ?></h2>
                  <div class="product_review">
                    <ul class="review_text list-inline">
                      <!--<li>
                        <div title="Rated 4.50 out of 5" class="star-rating"><span data-width="90%">4.50</span></div>
                      </li>
                      <li><a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a></li>-->
                      <!--<li><a href="#"><span>2</span>Reviews</a></li>-->
                      <!--<li><a href="#">Add reviews</a></li>-->
                    </ul>
                  </div>
                  <div class="price"> 
                    <?php if(!empty($d->precio_tachado)): ?>
                      <del>
                        <span class="amount"><?= $d->precio_tachado ?></span>
                      </del> 
                    <?php endif ?>
                    <ins>
                      <span class="amount"><?= moneda($d->precio) ?></span>
                    </ins> 
                  </div>
                  <div class="short-description">
                    <p><?= $d->descripcion ?></p>
                  </div>
                  <!--<div class="tags">
                    <strong>SKU:</strong> EA34
                  </div>-->
                  <div class="category">
                    <strong>Categoria:</strong> 
                    <a href="#">Jackets</a>, 
                    <a href="#">Shirts</a>
                  </div>
                  <div class="tags">
                    <strong>Tags:</strong> 
                    <a href="#"><?= $d->tags ?></a>                    
                  </div>
                  <div class="cart-form-wrapper mt-30">
                    <form enctype="multipart/form-data" method="post" class="cart">
                      <input type="hidden" value="productID" name="add-to-cart">                      
                      <button class="single_add_to_cart_button btn btn-default" type="submit"><i class="fa fa-shopping-cart"></i> Añadir al carro</button>
                    </form>
                  </div>
                </div>
              </div>
              <!--<div class="col-md-12">
                <div class="horizontal-tab product-tab">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>
                    <li><a href="#tab2" data-toggle="tab">Additional Information</a></li>
                    <li><a href="#tab3" data-toggle="tab">Reviews</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1">
                      <?php echo $d->descripcion ?>
                    </div>
                    <div class="tab-pane fade" id="tab2">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <th>Brand</th>
                            <td><p>Envato</p></td>
                          </tr>
                          <tr>
                            <th>Color</th>
                            <td><p>Black</p></td>
                          </tr>
                          <tr>
                            <th>Size</th>
                            <td><p>Large, Medium</p></td>
                          </tr>
                          <tr>
                            <th>Weight</th>
                            <td>27 kg</td>
                          </tr>
                          <tr>
                            <th>Dimensions</th>
                            <td>16 x 22 x 123 cm</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tab3">
                      <div class="reviews">
                        <ol class="commentlist">
                          <li class="comment">
                            <div class="media"> <a href="#" class="media-left"><img class="thumb img-circle media-object" alt="" src="[base_url]theme/theme/images/pages/75x75-1.png" width="75"></a>
                              <div class="media-body">
                                <ul class="review_text list-inline mt-5">
                                  <li>
                                    <div title="Rated 5.00 out of 5" class="star-rating"><span  data-width="100%">5.00</span></div>
                                  </li>
                                  <li>
                                    <h5 class="media-heading meta"><span class="author">Tom Joe</span> – Mar 15, 2015:</h5>
                                  </li>
                                </ul>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat purus tempor sem molestie, sed blandit lacus posuere. Lorem ipsum dolor sit amet.</div>
                            </div>
                          </li>
                          <li class="comment">
                            <div class="media"> <a href="#" class="media-left"><img class="thumb img-circle media-object" alt="" src="[base_url]theme/theme/images/pages/75x75-2.png" width="75"></a>
                              <div class="media-body">
                                <ul class="review_text list-inline mt-5">
                                  <li>
                                    <div title="Rated 4.00 out of 5" class="star-rating"><span  data-width="80%">4.00</span></div>
                                  </li>
                                  <li>
                                    <h5 class="media-heading meta"><span class="author">Mark Doe</span> – Jan 23, 2015:</h5>
                                  </li>
                                </ul>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat purus tempor sem molestie, sed blandit lacus posuere. Lorem ipsum dolor sit amet.</div>
                            </div>
                          </li>
                        </ol>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>-->
            <div class="col-md-12 mt-30">
              <h4>Related Products</h4>
              <div class="products related owl-carousel-4col" data-nav="true">
                <div class="item">
                  <div class="product">
                    <span class="tag-sale">Sale!</span>
                    <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo1.jpg" class="img-responsive img-fullwidth">
                      <div class="overlay"></div>
                    </div>
                    <div class="product-details text-center">
                      <a href="#"><h5 class="product-title">Cordless Combi Drill</h5></a>
                      <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                      <div class="star-rating" title="Rated 4.50 out of 5"><span data-width="90%">3.50</span></div>
                      <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>
                      <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="product">
                    <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo2.jpg" class="img-responsive img-fullwidth">
                      <div class="overlay"></div>
                    </div>
                    <div class="product-details text-center">
                      <a href="#"><h5 class="product-title">Angle Grinders</h5></a>
                      <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                      <div class="star-rating" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                      <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>
                      <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="product">
                    <span class="tag-sale">Hot!</span>
                    <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo3.jpg" class="img-responsive img-fullwidth">
                      <div class="overlay"></div>
                    </div>
                    <div class="product-details text-center">
                      <a href="#"><h5 class="product-title">Planers</h5></a>
                      <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                      <div class="star-rating" title="Rated 5.00 out of 5"><span  data-width="100%">3.90</span></div>
                      <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>
                      <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="product">
                    <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo4.jpg" class="img-responsive img-fullwidth">
                      <div class="overlay"></div>
                    </div>
                    <div class="product-details text-center">
                      <a href="#"><h5 class="product-title">Circular Saw</h5></a>
                      <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                      <div class="star-rating" title="Rated 4.90 out of 5"><span  data-width="95%">4.60</span></div>
                      <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>
                      <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="product">
                    <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo5.jpg" class="img-responsive img-fullwidth">
                      <div class="overlay"></div>
                    </div>
                    <div class="product-details text-center">
                      <a href="#"><h5 class="product-title">Angle Grinders</h5></a>
                      <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                      <div class="star-rating" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                      <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>
                      <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="product">
                    <span class="tag-sale">Hot!</span>
                    <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo6.jpg" class="img-responsive img-fullwidth">
                      <div class="overlay"></div>
                    </div>
                    <div class="product-details text-center">
                      <a href="#"><h5 class="product-title">Planers</h5></a>
                      <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                      <div class="star-rating" title="Rated 5.00 out of 5"><span  data-width="100%">3.90</span></div>
                      <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>
                      <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-heart"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
	

</div>
<!-- End main-content -->
[footer]