[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?= base_url() ?>theme/theme/images/pages/faq.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="<?= base_url() ?>theme/theme/images/pages/faq.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  FAQ
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	
    <section class="position-inherit">
      <div class="container">
        <div class="row">
          <div class="col-md-3 scrolltofixed-container">
            <div class="list-group scrolltofixed z-index-0">
              <a href="#section-one" class="list-group-item smooth-scroll-to-target"> Com funciona la targeta client? 
?</a>

              <a href="#section-two" class="list-group-item smooth-scroll-to-target">Què és l’Stop and go?</a>

              <a href="#section-three" class="list-group-item smooth-scroll-to-target">Què haig de fer si vull que em feu un xarop? unes càpsules? una crema?...</a>

              <a href="#section-four" class="list-group-item smooth-scroll-to-target">Què haig de fer quan em vull fer un test analític?
</a>

              <a href="#section-five" class="list-group-item smooth-scroll-to-target">T’han fet una intervenció i necessites temporalment unes crosses? Una cadira de rodes?
</a>
            </div>
          </div>
          <div class="col-md-9">
            <div id="section-one" class="mb-50">
              <h3>Com funciona la targeta client?</h3>
              <hr>
              <p class="mb-20">Acumula l'import de les compres en EFP i parafarmàcia, quan arribis a 200€ de compra t'enviem un xec de 6€ de regal que el podràs bescanviar en la propera compra de productes de parafarmàcia i EFP en qualsevol de les 647 farmàcies adherides a Xarxafarma d'arreu del país. Per consular les teves compres podràs fer-ho on line a través del teu compte client o baixante l’App de Xarxafarma.
            </div>
            <div id="section-two" class="mb-50">
              <h3>Què és l’Stop and go?</h3>
              <hr>
              <p class="mb-20">T’ho posem molt fàcil.<br>
Ens truques al 938034667 o ens envies un missatge de whatsapp a 696841650 amb el què t’interessa i a quina hora passaràs a buscar-ho. Nosaltres et prepararem la teva comanda perquè quan arribis a la farmàcia et prioritzarem el torn i així només hagis de pagar i marxar.
<br>Quan vinguis a recollir un encàrrec, no et preocupis et garantim que serà aparcar i marxar.</p>
              
            </div>
            <div id="section-three" class="mb-50">
              <h3>Què haig de fer si vull que em feu un xarop? unes càpsules? una crema?...
</h3>
              <hr>
              <p class="mb-20">Emplena el formulari de l’apartat de fórmules de la nostre web, ens envies una fotografia de la petició del teu prescriptor per whatsapp o bé ens la portes a la farmàcia i te la prepararem.
</p>
            </div>
            <div id="section-four" class="mb-50">
              <h3>Què haig de fer quan em vull fer un test analític?</h3>
              <hr>
              <p class="mb-20">No és necessari que demanis hora. El dia que et vagi bé fer-te la prova no esmorzis i passa per la farmàcia. Només necessitem una mostra molt petita de la teva sang que te la traurem del teu dit. Depenent del test ens caldrà un temps o altre per poder-te donar el resultat però no serà més de 24h.</p>
            </div>
            <div id="section-five" class="mb-50">
              <h3>T’han fet una intervenció i necessites temporalment unes crosses? Una cadira de rodes?</h3>
              <hr>
              <p class="mb-20">T’ho solucionem. A la farmàcia lloguem crosses i cadires de rodes per uns dies. Si ens truques t’informarem.
</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	

</div>
<!-- End main-content -->
[footer]