[menu]
<!-- Start main-content -->
<div class="main-content">

	<!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]img/blog/<?= $detail->slider_main ?>" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]img/blog/<?= $detail->slider_main ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  <?= $detail->titulo ?>
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>
    

		<!-- Section: Service details -->
	    <section>
	      <div class="container">
	        <div class="section-content">
	          <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-8 pull-left flip">
	              
	              <div class="row">
	                <div class="col-md-12">
	                 	<?= $detail->texto ?>
	                	<!-- <h2><span style="font-size: 14pt;">Les nostres especialistes</span></h2> -->
		                <?php if($detail->status==1): ?>
		                	  <?php $this->db->order_by('orden','ASC'); ?>
					          <?php 
					          	$this->db->select('b.*');
					          	$this->db->join('blog as b','b.id = blog.descripcion_corta');
					          	$equipo = $this->db->get_where('blog',array('b.idioma'=>$_SESSION['lang'],'blog.titulo'=>$detail->id));
					          	foreach($equipo->result() as $n=>$s): 			          							                
				                    $equipo->row($n)->link = base_url('equipo/'.toUrl($s->id.'-'.$s->titulo));
				                    $equipo->row($n)->foto = base_url('img/blog/'.$s->foto);
				                    $equipo->row($n)->foto_lateral = base_url('img/blog/'.$s->foto_lateral);                 
				                    $equipo->row($n)->texto = cortar_palabras(strip_tags($s->texto),10);				                
					          ?>
					            <!--<div class="col-md-4 pl-0">
					              <div class="box-hover-effect effect-alaska">
					                  <div class="effect-wrapper">
					                    <div class="thumb">
					                      <img class="img-fullwidth" src="<?= $s->foto ?>" alt="project">
					                    </div>
					                    <div class="bottom-info-box bg-theme-colored-transparent-9">
					                      <div class="title">                        
					                        <h3 class="title text-white mt-0 mb-10"><?= $s->titulo ?></h3>
					                        <a class="text-white" href="<?= $s->link ?>"><?= $s->descripcion_corta ?></a>
					                        <div class="invisible-parts text-white mt-20">					                          
					                          <a href="<?= $s->link ?>" class="btn btn-dark btn-theme-colored2">Veure més</a>
					                        </div>                        
					                      </div>
					                    </div>
					                  </div>
					              </div>
					            </div>-->
					          <?php endforeach ?>
		                <?php endif ?>
	                </div>	               
	              </div>
	              <div class="row">
	                <div class="col-md-12">
	                  <img alt="" src="<?= $detail->foto ?>" style="margin-bottom:30px;" />
	                </div>
	              </div>
	            </div>
	            <div class="col-sx-12 col-sm-12 col-md-4 sidebar pull-right flip">
	             <div class="widget hidden-xs hidden-sm">
	                <img src="<?= $detail->foto_lateral ?>" style="width:100%;">
	              </div>

	              <div class="widget">
	                <h3 class="line-bottom mt-0">Què més oferim</h3>
	                <div class="categories">
	                  <ul class="list angle-double-right list-border">
						<?php foreach($categorias->result() as $s): ?>
	                    	<li><a href="<?= $s->link ?>"><?= $s->titulo ?></a></li>
	                	<?php endforeach ?>	                    
	                  </ul>
	                </div>
	              </div>

                <div class="widget visible-xs visible-sm">
                  <img src="<?= $detail->foto_lateral ?>" style="width:100%;">
                </div>
	              


	              	              
				<?php if($detail->status==1): ?>
	              	<div class="widget">


						<?php echo $output ?>



	              	</div>
				  <?php endif ?>

				  <div class="widget">
				  	<a href="#" data-toggle="modal" data-target=".bs-example-modal-lg">
	                	<img src="<?= base_url() ?>img/325x325.gif" style="width:100%;">
	            	</a>
	              </div>
				  


	            </div>
	          </div>
	        </div>
	      </div>
	    </section>

 <!--start Clients Section-->
  <section class="divider parallax layer-overlay overlay-theme-colored2-9" data-parallax-ratio="0.7">
    <div class="container pt-60 pb-60">       
        <div class="section-title text-center mt-0">
          <div class="row">
             <div class="col-md-8 col-md-offset-2 text-white">
              <h2 class="mt-0 line-height-1 text-uppercase">Les nostres <span class="text-theme-colored2">marques</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="[base_url]theme/theme/images/title-icon3.png" alt="">
              </div>
              
            </div>
          </div>
          <div class="col-md-12" style="margin-top: 20px;">
                  <!-- Section: Clients -->
                  <div class="owl-carousel-6col transparent text-center owl-nav-top">
                    <?php foreach($detail->marcas->result() as $m): ?>
              <div class="item"> 
                <a href="#">
                  <img src="<?= $m->foto ?>" alt="">
                </a>
              </div>
                    <?php endforeach ?>
                  </div>
                </div>
        </div>
    </div>
  </section>

	

</div>
<!-- End main-content -->
[footer]