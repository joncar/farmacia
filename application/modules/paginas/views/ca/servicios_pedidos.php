<div class="border-1px p-20 mb-0" style="background: #f8f8f8;">
  <h3 class="text-theme-colored mt-0 pt-5">Sol.licitud d'elaboració de medicament</h3>
  <hr>    

    <div class="form-group" style="font-size:14px;">
      <div>Nom i Cognoms <small>*</small></div>
      <input name="nombre" type="text" placeholder="" class="form-control">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Telèfon <small>*</small></div>
      <input name="telefono" type="text" placeholder="" class="form-control">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Email <small>*</small></div>
      <input name="email" class="form-control email" type="email" placeholder="">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>C.P <small>*</small></div>
      <input name="cp" class="form-control email" type="text" placeholder="">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Adreça completa <small>*</small></div>
      <input name="direccion" class="form-control email" type="text" placeholder="">
    </div>
    <div class="form-group" style="font-size:14px;">
      <div>Ciutat <small>*</small></div>
      <input name="ciudad" class="form-control email" type="text" placeholder="">
    </div>

    <div class="form-group" style="font-size:14px;">
      <div>Província <small>*</small></div>
      <input name="provincia" class="form-control email" type="text" placeholder="">
    </div>

    <div class="form-group" style="font-size:14px;">
      <div class="mb-15">Adjuntar recepta mèdica en PDF o JPG</div>
      <?= $input_fields['receta1']->input ?>      
    </div>

    <div class="form-group" style="font-size:14px;">      
      <?= $input_fields['receta2']->input ?>      
    </div>

    <div class="form-group" style="font-size:14px;">      
      <?= $input_fields['receta3']->input ?>      
    </div>

    <!--<div class="form-group" style="font-size:14px;">
      <div>Recollida de Producte</div>      
    </div>
    <div class="row">
      <div class="col-sm-6">
          <div class="form-group" style="font-size:14px;">
            <div>Data <small>*</small></div>
            <input name="fecha" class="form-control date-picker" type="text" placeholder="">
          </div>
       </div>    
       <div class="col-sm-6">
          <div class="form-group" style="font-size:14px;">
            <div>Hora <small>*</small></div>
            <input name="hora" class="form-control time-picker" type="text" placeholder="">
          </div>
       </div>
    </div>-->
    
    <div class="form-group" style="font-size:14px;">
      <div>Comentaris o Suggeriments <small>*</small></div>
      <textarea name="comentarios" class="form-control required" rows="5" placeholder=""></textarea>
    </div>
  
    <div class="form-group" style="font-size:14px;">
        <div class="checkbox pl-20">
    	    <div style="font-size:12px; line-height:14px;">
    	      <input type="checkbox" name="aviso" value="1"> Accepto rebre informació comercial, fins i tot per correu electrònic.
    	    </div>
    	</div>
    	<div class="checkbox pl-20">
    	    <div style="font-size:12px; line-height:14px;">
    	      <input type="checkbox" name="politica" value="1"> He llegit i accepto els <a href="#" class="text-theme-colored">Termes i condicions de contractació</a> i la <a href="#" class="text-theme-colored">Política de Privacitat</a>
    	    </div>
    	</div>
    </div>

  <div id='report-error' style="display:none" class='alert alert-danger'></div>
  <div id='report-success' style="display:none" class='alert alert-success'></div>
    
    <div class="form-group">
      <?php
        foreach($hidden_fields as $hidden_field){
                echo $hidden_field->input;
        }
      ?>
      <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Enviar</button>
    </div>
</div>