[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="[base_url]theme/theme/images/pages/wishlist.png">
      <div class="container pt-200 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">Lista de favoritos</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Nosotros</a></li>
                <li class="active text-theme-colored">Lista de favoritos</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

	<section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <div class="products">
                <div class="row multi-row-clearfix">
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <span class="tag-sale">Sale!</span>
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo1.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Cordless Combi Drill</h5></a>
                        <div class="star-rating" title="Rated 4.50 out of 5"><span data-width="90%">3.50</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo2.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Angle Grinders</h5></a>
                        <div class="star-rating" title="Rated 5.00 out of 5"><span  data-width="100%">5.00</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <span class="tag-sale">Hot!</span>
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo3.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Planers</h5></a>
                        <div class="star-rating" title="Rated 5.00 out of 5"><span  data-width="100%">3.90</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo4.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Circular Saw</h5></a>
                        <div class="star-rating" title="Rated 4.90 out of 5"><span  data-width="95%">4.60</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <span class="tag-sale">Sale!</span>
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo5.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Sandar</h5></a>
                        <div class="star-rating" title="Rated 4.00 out of 5"><span  data-width="80%">4.00</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo6.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Nail Gun</h5></a>
                        <div class="star-rating" title="Rated 2.50 out of 5"><span  data-width="50%">2.50</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <span class="tag-sale">Sale!</span>
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo7.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Cordless Multi Screwdriver</h5></a>
                        <div class="star-rating" title="Rated 4.50 out of 5"><span  data-width="95%">4.50</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo8.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Hot Gun</h5></a>
                        <div class="star-rating" title="Rated 4.00 out of 5"><span  data-width="80%">4.00</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-4 mb-30">
                    <div class="product">
                      <div class="product-thumb"> <img alt="" src="[base_url]img/tienda/promo9.jpg" class="img-responsive img-fullwidth">
                        <div class="overlay"></div>
                      </div>
                      <div class="product-details text-center">
                        <a href="#"><h5 class="product-title">Cordless Oscillating Tools</h5></a>
                        <div class="star-rating" title="Rated 4.50 out of 5"><span  data-width="94%">4.80</span></div>
                        <div class="price"><del><span class="amount">18€</span></del><ins><span class="amount">20€</span></ins></div>                        
                       <div class="btn-add-to-cart-wrapper">
                          <span class="views" href="#" title="Vistas"><i class="fa fa-eye"></i> 0</span>
                          <a class="btn btn-default btn-xs btn-add-to-cart" href="#"><i class="fa fa-shopping-cart"></i> Añadir al carro</a>                          
                          <a class="follow" href="#" title="Añadir a la lista de deseos"><i class="fa fa-times"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <nav>
                      <ul class="pagination theme-colored">
                        <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">...</a></li>
                        <li> <a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

	

</div>
<!-- End main-content -->
[footer]