[menu]
<!-- Start main-content -->
<div class="main-content">

	
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="[base_url]theme/theme/images/pages/valores.png">
      <div class="container pt-200 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">Valors</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="#">Inici</a></li>
                <li><a href="#">Nosaltres</a></li>
                <li class="active text-theme-colored">Valors</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

	

	<!-- Section: about -->
    <section id="about">
      <div class="container pb-0">
        <div class="row">
          <div class="col-md-8 wow fadeInLeft animation-delay1">
            <h2 class="mt-0">El nostre instint,<span class="text-theme-colored2"> cuidar-te</span></h2>
            <h3 class="mt-0">La constància i els nostres valors ens fan ser millors</h3>
            <p>La nostra feina és la nostra passió i volem transmetre aquesta passió als nostres clients. Volem que els clients es sentin reclzats, informats i satisfets. Si ho aconseguim és el nostre gran èxit!</p>
            <div class="row mt-20 mb-sm-30">
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Col·laboració</h4>
                    <p class="">Treballem de forma conjunta amb els nostres clients i proveïdors</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Confiança</h4>
                    <p class="">El respecte, la integritat i la sinceritat ens porten al camí correcte</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Servei</h4>
                    <p class="">El notre personal i els nostres clients ens inspiren compromís i passió</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Dedicació</h4>
                    <p class="">Treballem amb rigor i simplicitat per aconseguir tot el que ens proposem</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 wow fadeInRight animation-delay4">
            <img src="[base_url]theme/theme/images/pages/xx.png" alt="">
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-theme-colored2-9" data-bg-img="[base_url]theme/theme/images/pages/1200x800-4.png" data-parallax-ratio="0.7">
      <div class="container pt-60 pb-60">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="1754" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Clients satisfets</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-rocket mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="675" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Productes</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-add-user mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="248" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Ventes online</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-global mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="24" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Hores al teu serveis</h5>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Departments -->
   <!-- 
 <section id="depertments" class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1 text-uppercase">Clinic <span class="text-theme-colored2">Departments</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-centent">
          <div class="row">
            <div class="col-md-12">
              <div class="horizontal-tab-centered">
                <div class="text-center">
                  <ul class="nav nav-pills mb-10">
                    <li class="active"> <a href="#tab11" class="" data-toggle="tab" aria-expanded="false"> Orthopaedics</a> </li>
                    <li class=""> <a href="#tab12" data-toggle="tab" aria-expanded="false"> Cardiology</a> </li>
                    <li class=""> <a href="#tab13" data-toggle="tab" aria-expanded="true"> Neurology</a> </li>
                    <li class=""> <a href="#tab14" data-toggle="tab" aria-expanded="false"> Dental</a> </li>
                    <li class=""> <a href="#tab15" data-toggle="tab" aria-expanded="false"> Haematology</a> </li>
                  </ul>
                </div>
                <div class="tab-content bg-white">
                  <div class="tab-pane fade in active" id="tab11">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Orthopaedic</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab12">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Cardiology</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-2.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab13">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Neurology</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-3.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab14">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Dental</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-4.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab15">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="service-content ml-10 ml-sm-0">
                          <h2 class="">Hemtology</h2>
                          <p class="lead">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid molestias.</p>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                          <div class="row mt-20 mb-sm-30">
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Emergency Care</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Operation Theater</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Outdoor Checkup</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                                <div>
                                  <h4 class="mt-0 mb-0">Cancer Service</h4>
                                  <p class="">Lorem ipsum dolor sit amet consec tetur elit</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="thumb mt-20">
                          <img class="img-fullwidth" src="[base_url]theme/theme/images/pages/445x445-5.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </section>
 -->

    <!--start Clients Section-->
    <!-- 
<section class="clients bg-theme-colored2">
      <div class="container pt-10 pb-10">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!~~ Section: Clients ~~>
              <div class="owl-carousel-6col transparent text-center owl-nav-top">
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-1.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-2.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-3.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-4.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-5.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-6.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-7.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-8.png" alt=""></a></div>
                <div class="item"> <a href="#"><img src="[base_url]theme/theme/images/pages/200x120-9.png" alt=""></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
 -->
  <!-- end main-content -->

</div>
<!-- End main-content -->
[footer]