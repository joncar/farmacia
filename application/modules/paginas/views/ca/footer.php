<!-- Footer -->
  <footer id="footer" class="footer">
    <div class="container pt-70 pb-40">
      <div class="row border-bottom-black">
        <div class="col-sm-6 col-md-5">
          <div class="widget dark">
            <img class="mt-10 mb-20" alt="" src="[base_url]theme/theme/images/logo-wide-white.png">
            <p>C/ Major, 95 Vilanova del Camí (BCN)</p>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored mr-5"></i> <a href="tel:+34938034667" class="text-gray">93 803 46 67 / 696 84 16 50</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored mr-5"></i> <a class="text-gray" href="mailto:farmacia@farmaciamassana.cat">farmacia@farmaciamassana.cat</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-colored mr-5"></i> <a class="text-gray" href="#">www.farmaciamassana.cat</a> </li>
            </ul>
          </div>
          <div class="col-xs-12 col-md-4" style="padding:0">
            <div class="widget dark">
              <p>Xarxes Socials</p>
              <ul class="styled-icons icon-dark icon-circled icon-sm">
                <li><a href="https://www.facebook.com/farmaciamassana/" target="_new"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/farmaciamassana/" target="_new"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-md-6" style="padding:0">
            <div class="widget dark">
            <p>Subscriu-te</p>
            <!-- Mailchimp Subscription Form Starts Here -->
            <form class="newsletter-form" action="paginas/frontend/subscribir" onsubmit="return sendForm(this,'#footersubs')">
              <div id="footersubs"></div>
              <div class="input-group">
                <input type="email" value="" name="email" placeholder="El teu Email" class="form-control input-lg font-13" data-height="36px" id="mce-EMAIL-footer">
                <span class="input-group-btn">
                  <button data-height="36px" class="btn btn-colored btn-theme-colored btn-xs m-0 font-13" type="submit" style="padding:3px;">Subscriure's</button>
                </span>
              </div>
            </form>
          </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-7">
          <div 
            id="map-canvas-multipointer"
            data-mapstyle="style10"
            data-height="334"
            data-zoom="17"
            data-lat="41.5714886"
            data-lon="1.6385783"
            data-title = "Farmacia Massana"
            data-content = "La teva farmàcia de confiança. Amb més de 50 anys d'experiència en farmàcia a Vilanova del Camí."
            data-marker="[base_url]img/map-marker.png">
          </div>
          <!-- Google Map Javascript Codes -->
          
        </div>        
      </div>
      
    </div>
    <div class="footer-bottom">
      <div class="container pt-10 pb-30">
        <div class="row">
          <div class="col-md-12 sm-text-center">
            <p class="font-13 text-black-777 m-0">Copyright &copy;<?= date("Y") ?> Farmacia Massana. Todos los derechos reservados | Por <a href="http://www.jordimagana.com">Jordi Magaña </a> | <a href="<?= base_url() ?>avis-legal.html">Avís Legal</a></p>
          </div>          
        </div>
      </div>
    </div>
  </footer>





  <!-- Large modal -->

  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg hidden-xs hidden-sm">

      <div class="modal-content">

        
        <button type="button" data-dismiss="modal" aria-label="Close" class="close mt-10 mr-20" style="position: absolute;right: 0;z-index:1"><span aria-hidden="true">×</span></button>
        <div class="megamenu megamenu-bg-img" style="background-image: url('[base_url]theme/theme/images/pages/megamenu-dropdown.jpg');background-position: -27px bottom;background-repeat: no-repeat; border-radius: 0 0 6px 6px">
          <div class="row ml-20 mr-20 mt-20">
            <!-- Titulos --->
            <div class="col-md-3 ml-0">
              <h4 class="megamenu-col-title">Fes la teva comanda pels diferents canals</h4>                      
            </div>
            <div class="col-md-3">
              <h4 class="megamenu-col-title">Atenem la teva comanda al moment</h4>                      
            </div>
            <div class="col-md-3">
              <h4 class="megamenu-col-title">Preparem la teva comanda en menys de 24 hores</h4>                      
            </div>
            <div class="col-md-3">
              <h4 class="megamenu-col-title">T'avisem i pots recollir la teva comanda</h4>                      
            </div>
            <!-- End titulos ---->
            <div class="col12 ml-0 mb-20">
              <img src="<?= base_url('img/megamenu.png') ?>" alt="" style="width:100%;">
            </div>
            <!--- Textos ---->
            <div class="col-md-3 ml-0">                      
              <p>Per telèfon al 93 803 46 67,<br>per whatsapp al 696 841 650 <br>o per email <a href="mailto:farmacia@farmaciamassana.cat" class="text-theme-colored">AQUÍ</a></p>
            </div>
            <div class="col-md-3">                      
              <p>Rebràs una resposta de que estem preparant la teva comanda.</p>
            </div>
            <div class="col-md-3">                      
              <p>En la data i hora pactada tindràs la teva comanda preparada.</p>
            </div>
            <div class="col-md-3">                      
              <p>Quan arribis et prioritzarem el torn i així només hauràs de pagar i marxar.</p>
            </div>
            
            <div class="col-md-12 mb-20 mt-20 hidden-sm hidden-xs">
              <h2 class="megamenu-col-title">Com funciona Stop&go?</h2> 
            </div>
           
            <!--- End textos ---->
          </div>
        </div>


      </div>
    </div>



    <div class="modal-dialog modal-md visible-xs visible-sm">

      <div class="modal-content">

        
        <button type="button" data-dismiss="modal" aria-label="Close" class="close mt-10 mr-20" style="position: absolute;right: 0;z-index:1"><span aria-hidden="true">×</span></button>
        <div class="megamenu megamenu-bg-img" style="background-image: url('[base_url]theme/theme/images/pages/megamenu-dropdown.jpg');background-position: -27px bottom;background-repeat: no-repeat; border-radius: 0 0 6px 6px">
          <div class="row ml-20 mr-20 mt-20">
            <!-- Titulos --->
            <div class="col-md-12 text-center">
              <h2 class="megamenu-col-title">Com funciona Stop&go?</h2> 
            </div>
            <div class="col-md-12 text-center">
              <h4 class="megamenu-col-title">Fes la teva comanda pels diferents canals</h4>                 
              <img src="<?= base_url('img/stop2go1.png') ?>" alt="">
              <p>Per telèfon al 93 803 46 67,<br>per whatsapp al 696 841 650 <br>o per email <a href="mailto:farmacia@farmaciamassana.cat" class="text-theme-colored">AQUÍ</a></p>                          
            </div>
            <div class="col-md-12 text-center">
              <h4 class="megamenu-col-title">Atenem la teva comanda al moment</h4>
              <img src="<?= base_url('img/stop2go2.png') ?>" alt="">                   
              <p>Rebràs una resposta de que estem preparant la teva comanda.</p>                 
            </div>
            <div class="col-md-12 text-center">
              <h4 class="megamenu-col-title">Preparem la teva comanda en menys de 24 hore
              <img src="<?= base_url('img/stop2go3.png') ?>" alt="">            s</h4>  
              <p>En la data i hora pactada tindràs la teva comanda preparada.</p>                      
            </div>
            <div class="col-md-12 mb-40 text-center">
              <h4 class="megamenu-col-title">T'avisem i pots recollir la teva comanda</h4>   
              <img src="<?= base_url('img/stop2go4.png') ?>" alt="">                  
              <p>Quan arribis et prioritzarem el torn i així només hauràs de pagar i marxar.</p>               
            </div>
           
            <!--- End textos ---->
          </div>
        </div>


      </div>
    </div>


  </div>