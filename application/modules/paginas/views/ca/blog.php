[menu]
<!-- Start main-content -->
<div class="main-content">

    <!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/blog.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/blog.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Blog
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>
   
    <section>
         <div class="container mt-30 mb-30 pt-30 pb-30">
           <div class="row blog-posts">
             <div class="col-md-12">
               <!-- Blog Masonry -->
               <div id="grid" class="gallery-isotope default-animation-effect grid-2 masonry gutter-30 clearfix">
                 <!-- grid sizer for Masonry -->
                 <div class="gallery-item gallery-item-sizer"></div>
                 
                 [foreach:blog]
                   <!-- Blog Item Start -->
                   <div class="gallery-item">
                     <article class="post clearfix mb-30 bg-lighter">
                       <div class="entry-header">
                         <div class="post-thumb thumb"> 
                           <img src="[foto]" alt="" class="img-responsive img-fullwidth"> 
                         </div>
                       </div>
                       <div class="entry-content border-1px p-20 pr-10">
                         <div class="entry-meta media mt-0 no-bg no-border">
                           <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                             <ul>
                               <li class="font-16 text-white font-weight-600">[dia]</li>
                               <li class="font-12 text-white text-uppercase">[mes]</li>
                             </ul>
                           </div>
                           <div class="media-body pl-15">
                             <div class="event-content pull-left flip">
                               <h4 class="entry-title text-white text-uppercase m-0 mt-5">
                                <a href="[link]">[titulo]</a>
                               </h4>
                               <!--<span class="mb-10 text-gray-darkgray mr-10 font-13">
                                <i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments
                                </span>-->
                               <span class="mb-10 text-gray-darkgray mr-10 font-13">
                                <i class="fa fa-eye mr-5 text-theme-colored"></i> [vistas]</span>
                             </div>
                           </div>
                         </div>
                         <p class="mt-10">[texto]</p>
                         <a href="[link]" class="btn-read-more">Veure més</a>
                         <div class="clearfix"></div>
                       </div>
                     </article>
                   </div>
                   <!-- Blog Item End -->
                 [/foreach]
                 

               </div>
               <!-- Blog Masonry -->
             </div>
           </div>
           <div class="row">
             <div class="col-sm-12">
               <nav>
                [paginacion]                 
               </nav>
             </div>
           </div>
         </div>
       </section> 

   

   

</div>
<!-- End main-content -->
[footer]