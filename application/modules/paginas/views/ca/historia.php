[menu]
<!-- Start main-content -->
<div class="main-content">

    <!-- Section: home -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/historia.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/historia.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['70','60','50','45']"
                  data-fontweight="['800','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Història <span class="text-theme-colored">i Valors</span>
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	

	<!-- Section: About -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <img class="pull-left flip mr-15 thumbnail" src="[base_url]theme/theme/images/pages/rb.png" alt="">
            <p><b>Tot comença l’any 1962,</b> quan  Josep Mª Massana barceloní, llicenciat en Farmàcia, Farmacologia, Hematologia i amb una inquietud constant per la física i l’astronomia, decideix establir-se a Vilanova del Camí que, en aquella època, tenia poc mes de 1.000 habitants, i a on els carrers encara estaven per asfaltar.
<br>S'instal·là en un local situat a la Plaça Major, acondicionat-lo amb un taulell de fusta i un petit laboratori per poder elaborar fórmules magistrals.
<br>El servei d'enviament diari dels medicaments des de Barcelona a Vilanova es feia a través dels ferrocarrils de la Generalitat, on el maquinista es feia responsable de guardar la caixa dels remeis durant tot el trajecte. 
<br><br><br><b>El 1977,</b> la farmàcia es trasllada al Carrer Major. A partir d’aleshores, gràcies als avanços científics, el ventall de molècules terapèutiques es va ampliar d’una manera espectacular per poder fer front a les diferents patologies.
<br> D'aquesta manera,  els vilanovins podien tenir els medicaments amb rapidesa professionalitat i començar molt aviat la teràpia prescrita pels especialistes assegurant aixi la remissió de les seves malalties.
<br>Amb els anys la formació ha estat, és i serà contínua en tots els sentits, tan a nivell de coneixements de salut com tecnològics.
<br>El nostre equip humà, format per farmacèutiques llicenciades a la facultat de Barcelona i tècnics formats a l’escola Milà i Fontanals, actualitza els coneixements (formulacions magistrals, farmacologia, homeopatia, plantes medicinals...) per aplicar-los a la prevenció i promoció de la salut.
</p>
<div class="clearfix"></div>
            
            <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
            <div class="row">
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/1.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/2.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/3.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/4.png" class="img-fullwidth"> </a> </div>
            </div>
   <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>         
  <!-- Section: about -->
    <section id="about">
      <div class="container pb-0">
        <div class="row">
          <div class="col-md-8 wow fadeInLeft animation-delay1">
            <h2 class="mt-0">El nostres<span class="text-theme-colored2"> valors</span></h2>
            <h3 class="mt-0">La constància i els nostres valors ens fan ser millors</h3>
            <p>La nostra feina és la nostra passió i volem transmetre aquesta passió als nostres clients. Volem que els clients es sentin recolzats, informats i satisfets. Si ho aconseguim és el nostre gran èxit!</p>
            <div class="row mt-20 mb-sm-30">
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-ambulance14"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Col·laboració</h4>
                    <p class="">Treballem de forma conjunta amb els nostres clients i proveïdors</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-illness"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Confiança</h4>
                    <p class="">El respecte, la integritat i la sinceritat ens porten al camí correcte</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-stethoscope10"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Servei</h4>
                    <p class="">El nostre personal i els nostres clients ens inspiren compromís i passió</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="service-icon-box"> <a href="#" class="icon pull-left mr-20"><i class="flaticon-medical-medical51"></i></a>
                  <div>
                    <h4 class="mt-0 mb-0">Dedicació</h4>
                    <p class="">Treballem amb rigor i simplicitat per aconseguir tot el que ens proposem</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 wow fadeInRight animation-delay4">
            <img src="[base_url]theme/theme/images/pages/xx.jpg" alt="">
          </div>
        </div>
      </div>
    </section>
    
            <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
            <h2 class="mt-0">Com ho<span class="text-theme-colored2"> volem fer?</span></h2>
                        <p>Amb el constant instint de cuidar-vos, fem de la vostre salut la nostra prioritat, posem al vostre servei tot el nostre coneixement i  experiència professional, perquè millorar la vostre qualitat de vida sigui el nostre sincer compromís, implicant-nos en la prevenció i desitjant donar-vos les solucions més adequades.
 <br><br> Tenim el repte de mantenir la vostra confiança i el deure de respectar el medi ambient sense perdre l’esperança d’aconseguir una societat més humana i solidària.

</p>
          </div>
          <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
            <div class="row" style="margin-left:0; margin-right:0">
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/5.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/6.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/7.png" class="img-fullwidth"> </a> </div>
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img alt="..." src="[base_url]theme/theme/images/pages/8.png" class="img-fullwidth"> </a> </div>
            </div>
            <div class="separator separator-rouned">
              <i class="fa fa-leaf fa-spin"></i>
            </div>
        </div>
      </div>
    </section>
  </div>

    <!-- Divider: Funfact -->
    <!-- 
<section class="divider parallax layer-overlay overlay-theme-colored2-9" data-bg-img="[base_url]theme/theme/images/pages/1200x800-4.png" data-parallax-ratio="0.7">
      <div class="container pt-60 pb-60">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="1754" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Clients satisfets</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-rocket mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="675" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Productes</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-add-user mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="248" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Ventes online</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-global mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="24" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h5 class="text-white text-uppercase font-weight-600">Hores al teu serveis</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
 -->

  <!-- end main-content -->

</div>
<!-- End main-content -->
[footer]