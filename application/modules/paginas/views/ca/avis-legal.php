[menu]
<!-- Start main-content -->
<div class="main-content">

	 
   <section id="home" class="divider">
      <div class="container-fluid p-0">

        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="[base_url]theme/theme/images/pages/aviso-legal.png" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="[base_url]theme/theme/images/pages/aviso-legal.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(255, 255,255, 0.1);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-black-333 rs-parallaxlevel-0" 
                  id="slide-1-layer-2" 
                  data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" 
                  data-y="['top','top','top','top']" data-voffset="['120','100','70','90']" 
                  data-fontsize="['56','46','40','36']"
                  data-lineheight="['50','60','50','45']"
                  data-fontweight="['700','700','700','700']"
                  data-width="['auto','650','600','420']"
                  data-height="none"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                  data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 6; padding: 30px; white-space: normal; background: rgba(255,255,255,0.8);">
                  Avís Legal
                                    
                </div>
              </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,                
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
      </div>
    </section>

	
	<!-- Section: Experts Details -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">            
            <div class="col-md-12">
              <h3 class="name font-24 mt-0 mb-0"></h3>
              <h4 class="mt-5 text-theme-colored">Qui és el responsable del tractament de les seves dades personals?</h4>
              <p>ROSER MASANA CLOSA (d'ara endavant, "FARMÀCIA MASSANA"), amb domicili al carrer Major, 95, 08788 Vilanova del Camí, (Barcelona), amb N.I.F. nº 46657606J, és la responsable del tractament de les dades personals que vostè ens proporciona i es responsabilitza de les referides dades personals d'acord amb la normativa aplicable en matèria de protecció de dades.</p>
              <h4 class="mt-5 text-theme-colored">On emmagatzemem les seves dades?</h4>
              <p>Les dades que recopilem sobre vostè s'emmagatzemen dins de l'Espai Econòmic Europeu («EEE»). Qualsevol transferència de les seves dades personals serà realitzada de conformitat amb les lleis aplicables.</p>
              <h4 class="mt-5 text-theme-colored">A qui comuniquem les seves dades?</h4>
              <p>Les seves dades poden ser compartides per FARMÀCIA MASSANA. Mai passem, venem ni intercanviem les seves dades personals amb terceres parts. Les dades que s'enviïn a tercers, s'utilitzaran únicament per oferir-li a vostè els nostres serveis. Li detallarem les categories de tercers que accedeixen a les seves dades per a cada activitat de tractament específica.</p>
              <h4 class="mt-5 text-theme-colored">Quina és la base legal per al tractament de les seves dades personals?</h4>
              <p>En cada tractament específic de dades personals recopilades sobre vostè, li informarem si la comunicació de dades personals és un requisit legal o contractual, o un requisit necessari per subscriure un contracte, i si està obligat a facilitar les dades personals, així com de les possibles conseqüències de no facilitar tals dades.</p>
              <h4 class="mt-5 text-theme-colored">Quins són els seus drets?</h4>
              <p>Dret d’accés: Qualsevol persona té dret a obtenir confirmació sobre si a FARMÀCIA MASSANA estem tractant dades personals que els concerneixin, o no. Pot contactar a FARMÀCIA MASSANA que li remetrà les dades personals que tractem sobre vostè per correu electrònic.</p>
              <p>Dret de portabilitat: Sempre que FARMÀCIA MASSANA processi les seves dades personals a través de mitjans automatitzats sobre la base del seu consentiment o a un acord, vostè té dret a obtenir una còpia de les seves dades en un format estructurat, d'ús comú i lectura mecànica transferida al seu nom o a un tercer. En ella s'inclouran únicament les dades personals que vostè ens hagi facilitat.</p>
              <p>Dret de rectificació: Vostè té dret a sol·licitar la rectificació de les seves dades personals si són inexactes, incloent el dret a completar dades que figurin incomplets.</p>
              <p>Dret de supressió: •	Vostè té dret a obtenir sense dilació indeguda la supressió de qualsevol dada personal seva tractada per FARMÀCIA MASSANA a qualsevol moment, excepte en les següents situacions:

              <br>* té un deute pendent amb FARMÀCIA MASSANA, independentment del mètode de pagament
              <br>* se sospita o està confirmat que ha utilitzat incorrectament els nostres serveis en els últims quatre anys
              <br>* ha contractat algun servei pel que conservarem les seves dades personals en relació amb la transacció per normativa comptable.
              </p>
              <p>Dret d’oposició al tractament de dades en base a l’interés legítim: Vostè té dret a oposar-se al tractament de les seves dades personals sobre la base de l'interès legítim de FARMÀCIA MASSANA. En aquest cas, FARMÀCIA MASSANA no seguirà tractant les dades personals tret que puguem acreditar motius legítims imperiosos per al tractament que prevalguin sobre els seus interessos, drets i llibertats, o bé per a la formulació, l'exercici o la defensa de reclamacions.</p>
              <p>Dret d’oposició al marketing directe: 
              Vostè té dret a oposar-se al màrqueting directe, incloent l'elaboració de perfils realitzada per a aquest màrqueting directe.
              Pot desvincular-se del màrqueting directe en qualsevol moment de les següents maneres: *seguint les indicacions facilitades a cada correu de màrqueting</p>
              <p>Dret a presentar una reclamació davant d’una autoritat de control: Si vostè considera que FARMÀCIA MASSANA tracta les seves dades d'una manera incorrecta, pot contactar amb nosaltres. També té dret a presentar una queixa davant l'autoritat de protecció de dades competent.</p>
              <p>Dret a limitació en el tractament: 
              Vostè té dret a sol·licitar que FARMÀCIA MASSANA limiti el tractament de les seves dades personals en les següents circumstàncies:
              <br>* si vostè s'oposa al tractament de les seves dades sobre la base de l'interès legítim de FARMÀCIA MASSANA. En aquest cas FARMÀCIA MASSANA haurà de limitar qualsevol tractament d'aquestes dades a l'espera de la verificació de l'interès legítim.
              <br>* si vostè reclama que les seves dades personals són incorrectes, FARMÀCIA MASSANA ha de limitar qualsevol tractament d'aquestes dades fins que es verifiqui la precisió dels mateixos.
              <br>* si el tractament és il·legal, vostè podrà oposar-se al fet que s'eliminin les dades personals i, en el seu lloc, sol·licitar la limitació del seu ús.
              <br>* si FARMÀCIA MASSANA ja no necessita les dades personals, però vostè els necessita per a la formulació, l'exercici o la defensa de reclamacions.</p>
              <p>Exercici de drets: 
              Ens prenem molt de debò la protecció de dades i, per tant, comptem amb personal de servei al client dedicat que s'ocupa de les seves sol·licituds en relació amb els drets abans esmentats. Sempre pot posar-se en contacte amb ells a farmacia@farmaciamassana.cat.

              </p>
              <ul class="styled-icons icon-dark icon-theme-colored2 icon-sm mt-15 mb-0">
               <!-- 
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
               <li><a href="#"><i class="fa fa-skype"></i></a></li>
               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
 -->
             </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
	

</div>
<!-- End main-content -->
[footer]