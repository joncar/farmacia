<?php

class Tienda_model extends CI_Model{

	const NOPROCESADO = '-1';
    const PORPROCESAR = '1';
    const PROCESADO = '2';


	function __construct(){
		parent::__construct();
		$this->load->model('carrito');
		$this->load->model('user');
	}	

	function cleanVentas(){
		foreach($this->db->get_where('ventas',array('user_id'=>$this->user->id,'procesado'=>self::PORPROCESAR))->result() as $v){
			$this->db->delete('ventas',array('id'=>$v->id));
			$this->db->delete('ventas_detalles',array('ventas_id'=>$v->id));
		}
	}

	function saveVenta(){
		if(!empty($_SESSION['carrito'])){
            $precio = 0;
            $cantidad = 0;
            $productos = '';
            $carrito = $this->carrito->getCarrito();
            foreach($carrito as $c){                
                $precio+= ($c->precio*$c->cantidad);
                $cantidad+= $c->cantidad;
                $productos.= $c->nombre_producto.', ';
            }
            $this->db->insert('ventas',
        		array(
	    			'cantidad'=>$cantidad,
	    			'productos'=>$productos,
	                'user_id'=>$this->user->id,
	                'fecha_compra'=>date("Y-m-d H:i:s"),
	                'precio'=>str_replace(',','.',$precio),
	                'cantidad'=>$cantidad,
	                'procesado'=>self::PORPROCESAR
            	)
        	);
            $id = $this->db->insert_id();
            foreach($carrito as $c){
                $this->db->insert('ventas_detalles',array(
                    'ventas_id'=>$id,
                    'productos_id'=>$c->id,
                    'cantidad'=>$c->cantidad,
                    'precio'=>$c->precio
                ));
            }
            return '000'.$id;

        }
	}

	function pagoOk($id){
        if(!empty($id)){                
            $this->db->select('ventas.id as nrocompra, ventas.precio as totalP, user.*, ventas.*, productos.*, ventas_detalles.cantidad as cantidadProd, ventas_detalles.id as idDetalle');
            $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
            $this->db->join('productos','ventas_detalles.productos_id = productos.id');
            $this->db->join('user','user.id = ventas.user_id','inner');
            $producto = $this->db->get_where('ventas',array('ventas.id'=>$id));
            if($producto->num_rows()>0){                                
                $notif = $producto->row();
                $notif->notificacion = str_replace('{datos}',$this->db->get_where('notificaciones',array('id'=>13))->row()->texto,$notif->notificacion);
                $notif->fecha_caducidad = date('d/m/Y',strtotime($notif->fecha_caducidad));
                $notif->fecha_nacimiento = date('d/m/Y',strtotime($notif->fecha_nacimiento));
                
                $prov = $producto->row()->provincias_id;
                if($prov=='15' || $prov=='26' || $prov=='35' || $prov=='52'){
                    $producto->row()->email_info = 'info@mallorcaislandfestival.com';                    
                }else{
                    $producto->row()->email_info = 'findecurso@mallorcaislandfestival.com';
                }
                if(!empty($notif->titulo_notificacion) && !empty($notif->notificacion)){
                    $notif = array('titulo'=>$notif->titulo_notificacion,'texto'=>$notif->notificacion);
                    get_instance()->enviarcorreo($producto->row(),$notif);
                    get_instance()->enviarcorreo($producto->row(),$notif,'reservas@mallorcaislandfestival.com');
                }else{
                    get_instance()->enviarcorreo($producto->row(),3);
                    get_instance()->enviarcorreo($producto->row(),4,'reservas@mallorcaislandfestival.com');
                }
                if($producto->row()->categorias_id!='1'){
                    get_instance()->enviarcorreo($producto->row(),4,'k.grados@finalia.es');
                }
            }
            $this->db->update('ventas',array('procesado'=>2),array('id'=>$id));
            unset($id);
        }
    }
    
    function pagoNoOk($id){
        if(!empty($id)){                            
            $this->db->update('ventas',array('procesado'=>-1),array('id'=>$id));
            unset($id);
        }
    }
}