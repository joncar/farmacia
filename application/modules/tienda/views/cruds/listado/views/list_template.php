<?php
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/flexigrid.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
/** Fancybox */
if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>

<div class="products flexigrid">
    <button class="ajax_refresh_and_loading" style="display: none;">Refresh</button>
    <div class="row multi-row-clearfix ajax_list">
        <?php echo $list_view ?>
    </div>


    <div class="row">
      <div class="col-md-12">
        <nav>
          <ul class="pagination theme-colored">
            <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>
            <li class="active"><a href="[base_url]producto.html">1</a></li>
            <li><a href="[base_url]producto.html">2</a></li>
            <li><a href="[base_url]producto.html">3</a></li>
            <li><a href="[base_url]producto.html">4</a></li>
            <li><a href="[base_url]producto.html">5</a></li>
            <li><a href="[base_url]producto.html">...</a></li>
            <li> <a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>
          </ul>
        </nav>
      </div>
    </div>



  </div>




<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>
