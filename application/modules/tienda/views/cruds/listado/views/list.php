<?php foreach ($list as $num_row => $row): ?>        
	<div class="col-sm-6 col-md-4 col-lg-4 mb-30">
        <div class="product">                      
          <div class="product-thumb"> 
          	<img alt="" src="<?php echo base_url('img/productos/'.$row->foto) ?>" class="img-responsive img-fullwidth">
            <div class="overlay"></div>
          </div>
          <div class="product-details text-center">
            <a href="<?php echo base_url('producte/'.toUrl($row->nombre)) ?>">
            	<h5 class="product-title"><?php echo $row->nombre ?></h5>
            </a>
            
            <div>
            	<span><?php echo $row->descripcion ?></span>
            </div>
            <div class="price">
            	<?php if(!empty($row->precio_tachado)): ?>
	            	<del>
	            		<span class="amount"><?= $row->precio_tachado ?></span>
	            	</del>
            	<?php endif ?>
            	<ins><span class="amount"><?php moneda($row->precio) ?></span></ins>
            </div>                        
            <div class="btn-add-to-cart-wrapper">
              <span class="views" href="#" title="Vistas">
              	<i class="fa fa-eye"></i> <?= $row->vistas ?>
              </span>
              <a class="btn btn-default btn-xs btn-add-to-cart" href="#">
              	<i class="fa fa-shopping-cart"></i> Añadir al carro
              </a>
              <a class="follow" href="#" title="Añadir a la lista de deseos">
              	<i class="fa fa-heart"></i>
              </a>
            </div>
          </div>
        </div>
  </div>
<?php endforeach ?>
<?php if(empty($list)): ?>
	Sin resultados para mostrar.
<?php endif ?>