<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">Filtros de fechas para exportar el reporte en excel</h1>
	</div>
	<div class="panel-body">
		<form action="<?= base_url('t/ventas') ?>" method="get">
			<div class="row">
				<div class="col-xs-12 col-sm-5">
					<input type="text" id="desde" name="desde" class="date form-control" placeholder="Desde" value="<?= @$_GET['desde'] ?>">
				</div>
				<div class="col-xs-12 col-sm-5">
					<input type="text" id="hasta" name="hasta" class="date form-control" placeholder="Hasta" value="<?= @$_GET['hasta'] ?>">
				</div>
				<div class="col-xs-12 col-sm-2">
					<button class="btn btn-primary btn-block" type="submit">Filtrar</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?= $output ?>
<?php $this->load->view('predesign/datepicker'); ?>
<script>
	$(".date").datepicker({        
        dateFormat: "dd/mm/yy",
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true
    });

    $(".datetime-input-clear").button();

    $(".datetime-input-clear").click(function(){
            $(this).parent().find(".datetime-input").val("");
            return false;
    });	

    <?php if(!empty($_GET['desde']) && !empty($_GET['hasta'])): ?>
    	var d = $("#desde").val();
    	var h = $("#hasta").val();
    	$(".filtering_form").append('<input type="hidden" name="desde" value="'+d+'">');
    	$(".filtering_form").append('<input type="hidden" name="hasta" value="'+h+'">');
    	$(".buttons-csv").attr('href','<?php 
    		$desde = date("Y-m-d",strtotime(str_replace('/','-',$_GET['desde'])));
    		$hasta = date("Y-m-d",strtotime(str_replace('/','-',$_GET['hasta'])));
    		echo base_url('t/ventas/export/'.$desde.'/'.$hasta);
    	?>');
	<?php endif ?>
</script>