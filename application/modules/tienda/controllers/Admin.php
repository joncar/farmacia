<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();            
        } 

        function perfil($x = '',$y = ''){
            if(is_numeric($y) && $y!=$this->user->id){
                redirect('cuenta');
            }else{
                if(empty($x) || $x=='list' || $x=='success'){
                    header("Location:".base_url('panel'));
                }
                $this->as['perfil'] = 'user';
                $this->norequireds = array('foto','email','status','admin');
                $crud = $this->crud_function($x,$y);
                $crud->unset_list();
                $crud->where('id',$this->user->id)
                     ->set_theme('bootstrap3');            
                //$crud->fields('nombre','apellido_paterno','email','password','foto','ciudad','provincias_id','codigo_postal','');
                $crud->field_type('status','invisible');
                $crud->field_type('email','invisible');
                $crud->field_type('password','password')
                     ->field_type('admin','invisible')
                     ->field_type('fecha_registro','invisible');
                $crud->set_field_upload('foto','img/fotos');
                $crud->callback_before_update(function($post,$primary){
                    if(!empty($primary) && get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                        $post['password'] = md5($post['password']);                
                    }                
                    return $post;
                });
                $crud->callback_after_update(function($post,$id){
                    get_instance()->user->login_short($id);
                });
                $crud->unset_jquery();
                $crud = $crud->render();
                $crud->view = 'cuenta';
                $crud->activeTab = 'data-tab="perfil"';
                $this->loadView($crud);
            }
        }
        
        function cuenta(){
            $this->as['cuenta'] = 'ventas';
        	$crud = $this->crud_function('','');
            $crud->where('user_id',$this->user->id);
            $crud->set_theme('bootstrap3')
                 ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_delete()
                ->unset_export();
            $crud->unset_columns('user_id');            
            $crud->display_as('productos_id','Producto');
            $crud->columns('id','productos','precio','cantidad','fecha_compra','procesado');
            $crud->display_as('id','Nro. Compra')
                 ->display_as('user_id','Usuario');
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('store/pagar_articulo/'.$row->id).'" class="sr-button button-1 button-tiny">Por procesar</a>'; break;
                    case '2': return '<span class="label label-success">Procesado</span>'; break;
                }
            });
            $crud->unset_jquery();
            $crud->callback_before_delete(function($primary){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$primary));
                if($producto->num_rows>0){
                    if($producto->row()->procesado!=1){
                        return false;
                    }
                }
            });
            $crud->set_url('t/admin/cuenta/');
            $crud = $crud->render();
            $crud->view = 'cuenta';
            $crud->activeTab = 'data-tab="cuenta"';
            $this->loadView($crud);
        }
    }
