<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('tienda_model');
            $this->load->library('form_validation');
        } 
        
        function tienda(){
            $this->loadView(array('view'=>'tienda','title'=>'Reserva','url'=>base_url('store')));
        }

        function listado(){
            $this->load->library('grocery_crud')
                       ->library('ajax_grocery_crud');
            $crud = new ajax_grocerY_crud();
            $crud->set_table('productos')
                 ->set_subject('Producto')
                 ->set_theme('listado');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud->set_url('tienda/frontend/listado/');            
            $crud = $crud->render('','application/modules/tienda/views/cruds/');
            $crud->view = 'listado';
            $crud->title = 'Botiga';
            $this->loadView($crud);
        }

        function producto($producto){
            /*$ur = $producto;
            $id = explode('-',$producto);
            $id = $id[0];*/
            $id = $producto;
            if(!empty($id)){                
                $producto = $this->db->get_where('productos',array('limpiar(nombre)'=>$id));
                if($producto->num_rows()==0){
                    throw new exception('Producto no encontrado',404);
                }else{
                    $producto = $producto->row();
                    $this->loadView(array('view'=>'producto','producto'=>$producto,'title'=>$producto->nombre));
                }                
                
            }else{
                redirect('botiga');
            }
        }


        function ver_carro(){
            $this->loadView('carrito');
        }

        function checkout(){
            $carrito = $this->carrito->getCarrito(); 
            if(count($carrito)==0){
                redirect('store/carrito');
            }else{                
                if($this->user->log){

                    //Guardamos en la bd
                    $this->tienda_model->cleanVentas();
                    $id = $this->tienda_model->saveVenta();
                    $this->pagar_articulo($id);
                }else{
                    $this->tienda_model->cleanVentas();
                    redirect('registro/index/add?redirect=store/checkout');
                }
            }

        }

        function pagar_articulo($id){
            $this->db->select('ventas_detalles.*, productos.categorias_id');
            $this->db->join('productos','productos.id = ventas_detalles.productos_id');
            $detalle = $this->db->get_where('ventas_detalles',array('ventas_id'=>$id));
            if($detalle->num_rows()>0){
                $total = 0;                     
                foreach($detalle->result() as $c){
                    /*$total+= ($c->cantidad*$c->precio)+($c->cantidad*3.50);*/
                    $total+= ($c->cantidad*$c->precio);
                }
                $total+=(3.50); 
                /*if($this->user->id==2){
                    $total = 0.01;
                } */                  
                $miObj = new RedsysAPI;
                // Valores de entrada                    
                if($detalle->row()->categorias_id=='1'){
                    $merchantCode   ="091995811";
                    $key = 'KBagCRmyFKbAAYqRu2FCfOOlpyezifpy';//Clave secreta del terminal
                }else{
                    $merchantCode   ="327977559";
                    $key = 'xqSeBn4Qp+EsCTtko9MwNEGqFnMoDj2d';
                }
                $terminal       ="1";
                $amount         =$total*100;
                $currency       ="978";
                $transactionType    ="0";
                $merchantURL    =base_url('tt/procesarPago');
                $urlOK      =base_url('store/pagoOk/'.$id);
                $urlKO      =base_url('store/pagoKo/'.$id);
                $order      = $id.'-'.date("s");
                $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
                $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
                $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
                $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
                $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
                $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
                $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
                $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);       
                $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
                $version="HMAC_SHA256_V1";                    
                $request = "";
                $params = $miObj->createMerchantParameters();
                $signature = $miObj->createMerchantSignature($key);
                echo '<div style="text-align:center">Redireccionando al banco, por favor espere</div>';
                $this->load->view('checkout',array('version'=>$version,'params'=>$params,'signature'=>$signature));
                if(!empty($_SESSION['carrito'])){
                    unset($_SESSION['carrito']);
                }
            }else{
                redirect('carrito');
            }
        }

        public function refreshCartForm(){
            $this->load->view('includes/fragmentos/carritoForm');
        }
        
        public function carrito($venta_id = ''){
            $this->loadView('carrito');
        }

        public function addToCart($prod = '',$cantidad = '',$return = TRUE,$sumarcantidad = TRUE){
            if(!empty($prod) && is_numeric($prod) && $prod>0 && is_numeric($cantidad) && $cantidad>0){                
                $sumarcantidad = $sumarcantidad=='0'?FALSE:$sumarcantidad;
                $this->db->where('productos.id',$prod);
                $producto = $this->carrito->getProductos();
                if($producto->num_rows()>0){
                    $producto = $producto->row();
                    $producto->cantidad = $cantidad;
                    $this->carrito->setCarrito($producto,$sumarcantidad);
                }                
            }
            if($return){
                $this->load->view('_carrito');
            }
        }
        
        public function addToCartArray(){
            if(!empty($_POST)){
                $this->form_validation->set_rules('provincias_id','Provincia','required')
                                      ->set_rules('dia_entrega','Dia de entrega','required')
                                      ->set_rules('hora_entrega','Hora de entrega','required')
                                      ->set_rules('forma_pago','Forma de pago','required')
                                      ->set_rules('costo_envio','Costo de envio','required')
                                      ->set_rules('politicas','Costo de envio','required');
                if($this->form_validation->run()){
                    $response = 'success';
                    foreach($_POST['id'] as $n=>$v){
                        if(empty($_POST['gramaje'][$n]) && !empty($this->db->get_where('productos',array('id'=>$v))->row()->gramaje)){
                            $response = "Sis plau tria un gramatge";
                        }else{
                            $this->addToCart($v,$_POST['cantidad'][$n],$_POST['gramaje'][$n],FALSE,FALSE);
                        }
                    }
                    //Almacenar datos de envio
                    $_SESSION['envio']['provincias_id'] = $_POST['provincias_id'];
                    $_SESSION['envio']['dia_entrega'] = $_POST['dia_entrega'];
                    $_SESSION['envio']['hora_entrega'] = $_POST['hora_entrega'];
                    $_SESSION['envio']['forma_pago'] = $_POST['forma_pago'];
                    $_SESSION['envio']['observaciones'] = $_POST['observaciones'];
                    $_SESSION['envio']['costo_envio'] = $_POST['costo_envio'];
                    $total = 0;
                    foreach($_SESSION['carrito'] as $n=>$v){                        
                        $total+= ($v->precio*$v->cantidad);
                    }
                    $response = $total<45?'comanda minima de 45€':$response;
                    echo $response;
                }else{
                    echo "Sis plau completi totes les dades amb asterisc";
                }
            }else{
                echo "Sis plau completi totes les dades amb asterisc";
            }
        }
        
        public function delToCart($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view('_carritoForm');
        }

        public function delToCartNav($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view('_carrito');   
        }

        function getForm(){
            $this->load->view('_carritoForm');
        }

        function procesarPago(){             
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;

            //$this->tienda_model->pagoOk(47271);

            /*$_POST = array(
                'Ds_SignatureVersion' => 'HMAC_SHA256_V1',
                'Ds_Signature' => 'Vr71Chc4xojSd-ibNt3i_szcWsWAMMRL4yBBlUVRn2U=',
                'Ds_MerchantParameters' => 'eyJEc19EYXRlIjoiMDJcLzEwXC8yMDE4IiwiRHNfSG91ciI6IjExOjUzIiwiRHNfU2VjdXJlUGF5bWVudCI6IjEiLCJEc19DYXJkX1R5cGUiOiJDIiwiRHNfQ2FyZF9Db3VudHJ5IjoiNzI0IiwiRHNfQW1vdW50IjoiMSIsIkRzX0N1cnJlbmN5IjoiOTc4IiwiRHNfT3JkZXIiOiI0ODM2My0xMTUxNDEiLCJEc19NZXJjaGFudENvZGUiOiIzMjc5Nzc1NTkiLCJEc19UZXJtaW5hbCI6IjAwMSIsIkRzX1Jlc3BvbnNlIjoiMDAwMCIsIkRzX01lcmNoYW50RGF0YSI6IiIsIkRzX1RyYW5zYWN0aW9uVHlwZSI6IjAiLCJEc19Db25zdW1lckxhbmd1YWdlIjoiMSIsIkRzX0F1dGhvcmlzYXRpb25Db2RlIjoiOTA0MjMzIiwiRHNfQ2FyZF9CcmFuZCI6IjEifQ=='
            );*/
            if (!empty($_POST)){                
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));                                
                //$decodec = $_POST["Ds_MerchantParameters"];  //Eliminar siempre que se pase a produccion                              
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){
                    //$id = substr($decodec->Ds_Order,3);    
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                                    
                    $this->tienda_model->pagoOk($id);
                }else{
                    //$id = substr($decodec->Ds_Order,3);  
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                  
                    $this->tienda_model->pagoNoOk($id);
                }
                
                
                //correo('joncar.c@gmail.com','POST',print_r($decodec,TRUE));
            }
        }

        function pagoOk($id = ''){
            $this->loadView(array('view'=>'pagoOk','title'=>'Resultado de compra'));
        }

        function pagoKo($id = ''){
            $this->loadView(array('view'=>'pagoKo','title'=>'Resultado de compra'));
        }
    }
