<ul class="pagination theme-colored xs-pull-center m-0">
	<li><a href="<?= base_url('blog') ?>?page=1" class="prev-page"><span aria-hidden="true">«</span></a></li>

	<?php for($i=1;$i<=$total_pages;$i++): ?>
	  
	    <li class="<?= $i==$current_page?'active':'' ?>">
	    	<a href='<?= base_url('blog') ?>?page=<?= $i ?>' class="">
	      		<?= $i ?>
			</a>
		</li>
	  
	<?php endfor ?>
	<li>
		<a href="<?= base_url('blog') ?>?page=<?= $total_pages ?>" class="next-page">
			<span aria-hidden="true">»</span>
		</a>
	</li>
</ul>