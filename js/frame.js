

    function emergente(data,xs,ys,boton,header){


        var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
        var y = (ys==undefined)?(window.innerHeight/2):ys;
        var b = (boton==undefined || boton)?true:false;
        var h = (header==undefined)?'Mensaje':header;
        if($(".modal").html()==undefined){
        $('body,html').animate({scrollTop: 0}, 800);
        var str = '';
                var str = '<!-- Modal -->'+
                '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
                '<div class="modal-dialog">'+
                '<div class="modal-content">'+
                '<div class="modal-body">'+
                data+
                '</div>'+
                '<div class="modal-footer">'+
                '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
                '</div>'+
                '</div><!-- /.modal-content -->'+
                '</div><!-- /.modal-dialog -->'+
                '</div><!-- /.modal -->';
                $("body").append(str);
                $("#myModal").modal("toggle");
        }
        else
        {
            $(".modal-body").html(data);
                        if($("#myModal").css('display')=='none')
                            $("#myModal").modal("toggle");
        }
    }

    function remoteConnection(url,data,callback){        
        var r;
        if(typeof(grecaptcha)!=='undefined'){
            grecaptcha.ready(function() {
                var r = grecaptcha.execute('6Lc50o8UAAAAAPxrPuFAy2MlbQ-GszUJT_TpM-0Z', {action: 'action_name'})
                .then(function(token) {
                    // Verifica el token en el servidor.
                        data.append('token',token);
                        return $.ajax({
                        url: URL+url,
                        data: data,
                        context: document.body,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success:callback
                    });
                });
            });

        }else{
            r = $.ajax({
                url: URL+url,
                data: data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:callback
            });
        }
        return r;
    };

    function sendForm(form,responseDiv){
        var url = $(form).attr('action');
        var response = $(form).find(responseDiv);
        var data = new FormData(form);
        remoteConnection(url,data,function(data){
            if(response!==undefined){
                response.html(data);
            }else{
                console.log(response);
            }
        });
        return false;
    }
    