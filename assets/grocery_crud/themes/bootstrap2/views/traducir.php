<?php
$this->set_css($this->default_theme_path . '/bootstrap2/bootstrap/css/bootstrap.css');
$this->set_css($this->default_theme_path . '/bootstrap2/css/flexigrid.css');
$this->set_js($this->default_theme_path . '/bootstrap2/bootstrap/js/bootstrap.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap2/js/jquery.form.js');
$this->set_js_config($this->default_theme_path . '/bootstrap2/js/flexigrid-edit.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
?>

<?php     
    $idiomas = get_instance()->db->get_where('ajustes')->row()->idiomas; 
    $idiomas = explode(', ',$idiomas);  
    $next = array();
    for($i=1;$i<count($idiomas);$i++){
        $next[] = $idiomas[$i];
    }
    echo '<script>var idiomas = '.json_encode($next).'; var texts = [];</script>';
?>


<div class=" flexigrid crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open($update_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach($idiomas as $i=>$n): ?>
            <li role="presentation" class="<?= $i==0?'active':'' ?>">
                <a href="#tab<?= $n ?>" aria-controls="home" role="tab" data-toggle="tab"><?= $n ?></a>
            </li>
        <?php endforeach ?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <?php foreach($idiomas as $i=>$idi): ?>
        <div role="tabpanel" class="tab-pane <?= $i==0?'active':'' ?>" id="tab<?= $idi ?>">
            
            <?php $counter = 0; foreach ($fields as $field):
                $even_odd = $counter % 2 == 0 ? 'odd' : 'even';
                $counter++;
                if($i>0){
                    $input = str_replace($field->field_name, $field->field_name . '_'.$idi, $input_fields[$field->field_name]->input);
                    if (!empty($field_values->{$idi}->{$field->field_name})) {
                        $input = str_replace('value=""', 'value=\'' . $field_values->{$idi}->{$field->field_name} . '\'', $input);
                        $input = str_replace('</textarea>', $field_values->{$idi}->{$field->field_name} . '</textarea>', $input);
                    }
                }else{
                    $input = str_replace($field->field_name, '', $input_fields[$field->field_name]->input);                    
                    if (!empty($values->{$field->field_name})) {
                        $input = str_replace('value=""', 'value=\'' . $values->{$field->field_name} . '\'', $input);
                        $input = str_replace('</textarea>', $values->{$field->field_name} . '</textarea>', $input);
                    }
                    $input = str_replace('texteditor','',$input);
                    $input = str_replace('>',' readonly="true">',$input);
                }
                $name = str_replace($field->field_name, $field->field_name . '_'.$idi, $field->field_name);
            ?>

                <div class='form-group' id="<?php echo $name; ?>_field_box">
                    <label for='field-<?= $name ?>' id="<?php echo $name; ?>_display_as_box"  style="width:100%">
                        <?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required) ? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input ?>
                </div>
            <?php endforeach; ?>
           
        </div>
         <?php endforeach; ?>
    </div> 

    <div id='report-error' style="display:none" class='alert alert-danger'></div>
    <div id='report-success' style="display:none" class='alert alert-success'></div>
    <div class="btn-group">         
        <button id="form-button-save" type='submit' class="btn btn-success"><?php echo $this->l('form_save'); ?></button>
        <?php if (!$this->unset_back_to_list) { ?>
            <button type='button' id="save-and-go-back-button"  class="btn btn-default"><?php echo $this->l('form_save_and_go_back'); ?></button>                            
            <button type='button' id="cancel-button"  class="btn btn-danger"><?php echo $this->l('form_cancel'); ?></button>
        <?php } ?>
    </div>
    <?php echo form_close(); ?>
</div>
<script>

    var validation_url = '<?php echo $validation_url ?>';

    var list_url = '<?php echo $list_url ?>';



    var message_alert_edit_form = "<?php echo $this->l('alert_edit_form') ?>";

    var message_update_error = "<?php echo $this->l('update_error') ?>";

    $("input[type='text'],input[type='password']").addClass('form-control');
    $(document).on('ready', function () {
        $('#tab<?= $next[0] ?>').tab('show')
    });
</script>